# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-03 15:22+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: core.py:31
#, python-brace-format
msgid "Theme {theme} unknown ; defaulting to system theme {bot.themes.default}"
msgstr ""

#: core.py:33 core.py:461
#, python-brace-format
msgid "Available themes: {themes_list}"
msgstr ""

#: core.py:41
msgid "Name of the game"
msgstr ""

#: core.py:41
msgid "My new game"
msgstr ""

#: core.py:42
msgid ""
"Tag of the game (quick identifier, 5 characters min, for example "
"'v5montreal')"
msgstr ""

#: core.py:42
msgid "newgame"
msgstr ""

#: core.py:43
msgid "Main gamemaster"
msgstr ""

#: core.py:44
msgid "Gamemaster role"
msgstr ""

#: core.py:45
msgid "Bot theme"
msgstr ""

#: core.py:46
msgid "ID of the logging channel"
msgstr ""

#: core.py:56
#, python-brace-format
msgid "Panopticon v.{__version__} starting..."
msgstr ""

#: core.py:59
msgid "Initialization failed, aborting."
msgstr ""

#: core.py:72
msgid "Panopticon v2 - your friendly neighborhood virtual LARP gamemaster bot"
msgstr ""

#: core.py:72
#, python-brace-format
msgid "Type {self.prefix}help in DM"
msgstr ""

#: core.py:90
#, python-brace-format
msgid ""
"WARNING: Config file with unknown version {version}, this may cause issues"
msgstr ""

#: core.py:97
#, python-brace-format
msgid "Error : Missing critical configuration item {k}"
msgstr ""

#: core.py:110
#, python-brace-format
msgid ""
"Successfully connected to MongoDB server at {self.mongodb_uri} (server "
"version {v})"
msgstr ""

#: core.py:112
#, python-brace-format
msgid "Can't connect to MongoDB server at {self.mongodb_uri} !"
msgstr ""

#: core.py:115
#, python-brace-format
msgid "Trying to load module {module}... "
msgstr ""

#: core.py:119
msgid "OK"
msgstr ""

#: core.py:121
msgid "Module not found !"
msgstr ""

#: core.py:124
#, python-brace-format
msgid "Modules loaded: {modlist}"
msgstr ""

#: core.py:126
#, python-brace-format
msgid "Panopticon2 v.{__version__} ready."
msgstr ""

#: core.py:127
#, python-brace-format
msgid ""
"**__Panopticon2 v.{__version__} ready.__**\n"
"Startup finished, system ready :robot::game_die:"
msgstr ""

#: core.py:164
msgid ""
"You can only register players for games for which the server setup is "
"finished."
msgstr ""

#: core.py:290
#, python-brace-format
msgid "Welcome on the server for the game {gamename}, {member.name} !"
msgstr ""

#: core.py:328
#, python-brace-format
msgid ""
"Hello, {ctx.author.name}. You currently do not take part to any game handled "
"by this service :no_entry_sign::game_die:"
msgstr ""

#: core.py:336
#, python-brace-format
msgid ""
"Hello, {ctx.author.name}. You are currently part of the following game(s) :"
"game_die:: {gamestr}"
msgstr ""

#: core.py:339
#, python-brace-format
msgid "To change the selected game, use `{self.prefix}switch <gametag>`"
msgstr ""

#: core.py:342
msgid "No game is currently selected."
msgstr ""

#: core.py:350
#, python-brace-format
msgid ""
"\n"
"\n"
"**The currently selected game is: __{gamename}__.**"
msgstr ""

#: core.py:350
msgid " **You are a gamemaster ! :mage:**"
msgstr ""

#: core.py:362
msgid "Please use this command in DM."
msgstr ""

#: core.py:364 core.py:401 core.py:515 modules/codenames.py:160
#: modules/messaging.py:241 modules/spying.py:266 modules/tietextvoice.py:245
#, python-brace-format
msgid "Unknown error : {error}"
msgstr ""

#: core.py:383
#, python-brace-format
msgid "No known game with tag `{gametag}`."
msgstr ""

#: core.py:388
msgid "You are not part of this game."
msgstr ""

#: core.py:394
#, python-brace-format
msgid ""
"**{gamename}** is now the currently selected game ! *DMs with the bot will "
"refer to that game.*"
msgstr ""

#: core.py:399
msgid ""
"Please use this command in DM. The active game is only relevant to DMs, if "
"you talk with the bot on a server it will always refer to the game on the "
"current server."
msgstr ""

#: core.py:407
msgid "Sets up the bot on a new server"
msgstr ""

#: core.py:407
msgid "Launches the server setup process if this server is not already known."
msgstr ""

#: core.py:411
msgid "The bot is already set up (or being set up) for this server"
msgstr ""

#: core.py:413
msgid "**Bot setup started for this server.** You will be the main gamemaster."
msgstr ""

#: core.py:421
#, python-brace-format
msgid ""
"Use `{self.prefix}setup print` to print the current setup, `{self.prefix}"
"setup set <parameter> <value>` to set a parameter, `{self.prefix}setup "
"finish` to finish the setup and launch the game."
msgstr ""

#: core.py:431
#, python-brace-format
msgid "Unknown parameter {param}"
msgstr ""

#: core.py:440
#, python-brace-format
msgid "`{param}` ({translatedname}) is now set to **{newval}**"
msgstr ""

#: core.py:442
#, python-brace-format
msgid "Wrong type for parameter `{param}` (should be {conversion.__name__})"
msgstr ""

#: core.py:444
#, python-brace-format
msgid "Invalid argument for parameter `{param}` : {e}"
msgstr ""

#: core.py:446
msgid "Critical parameters can only be changed during game setup."
msgstr ""

#: core.py:455
msgid "**Current game parameters:**"
msgstr ""

#: core.py:463
#, python-brace-format
msgid ""
"No game is known on this server; run `{self.prefix}setup` to setup a new "
"game."
msgstr ""

#: core.py:475
msgid "Please set a GM role (`gm_role_id`)"
msgstr ""

#: core.py:478
msgid "Please set a bot log channel for this server (`log_channel_id`)"
msgstr ""

#: core.py:481
msgid "Please set a game tag (`tag`)"
msgstr ""

#: core.py:488
#, python-brace-format
msgid ""
"Game tag `{gametag}` is already used by another game, please set another one."
msgstr ""

#: core.py:490
msgid "Server setup complete ! "
msgstr ""

#: core.py:494
msgid "Registering all current non-bot users..."
msgstr ""

#: core.py:500
#, python-brace-format
msgid "Done ; {count_users} registered."
msgstr ""

#: core.py:502
msgid "Server must be in setup for setup to be finished."
msgstr ""

#: core.py:509 modules/codenames.py:158 modules/messaging.py:239
#: modules/spying.py:264 modules/tietextvoice.py:243
msgid "You need gamemaster rights to do this"
msgstr ""

#: core.py:511
msgid "You can only run setup commands on a server, not in DM."
msgstr ""

#: core.py:513
msgid "You can only run this in DM, not on a server."
msgstr ""

#: modules/codenames.py:13
msgid "Codenames for anonymous messaging or other operations"
msgstr ""

#: modules/codenames.py:65
#, python-brace-format
msgid ""
"Use `{self.bot.prefix}codename list` to list all codenames, `{self.bot."
"prefix}codename add <player> <codename>` to create a new codename, `{self."
"bot.prefix}codename delete <codename>` to remove a codename, or `{self.bot."
"prefix}codename info <codename>` to know who has a codename."
msgstr ""

#: modules/codenames.py:77 modules/codenames.py:111
#, python-brace-format
msgid "Unknown member {who}"
msgstr ""

#: modules/codenames.py:82
#, python-brace-format
msgid "The member **{who}** has codename(s) : {names_str}"
msgstr ""

#: modules/codenames.py:84
#, python-brace-format
msgid "The member **{who}** has no codename."
msgstr ""

#: modules/codenames.py:88
msgid "There are no codenames for anyone in this game."
msgstr ""

#: modules/codenames.py:97
msgid "Codenames for this game :"
msgstr ""

#: modules/codenames.py:115
#, python-brace-format
msgid ""
":pen_fountain: You can now be contacted through the **secret codename : "
"{codename}**"
msgstr ""

#: modules/codenames.py:116
#, python-brace-format
msgid ""
":pen_fountain: The codename {codename} has been attributed to {member."
"display_name}. They have been notified."
msgstr ""

#: modules/codenames.py:117
#, python-brace-format
msgid ""
":pen_fountain: I **added the codename {codename}** to **{member.display_name}"
"**"
msgstr ""

#: modules/codenames.py:119
msgid "This codename is already used or invalid."
msgstr ""

#: modules/codenames.py:129 modules/codenames.py:145
#, python-brace-format
msgid "Codename {codename} does not exist."
msgstr ""

#: modules/codenames.py:133
#, python-brace-format
msgid ""
":pen_fountain::no_entry_sign: You **can no longer be contacted** through the "
"secret codename : {codename}"
msgstr ""

#: modules/codenames.py:134
#, python-brace-format
msgid ""
":pen_fountain::no_entry_sign: The codename {codename} has been removed from "
"{member.display_name}. They have been notified."
msgstr ""

#: modules/codenames.py:135
#, python-brace-format
msgid ""
":pen_fountain::no_entry_sign: I **removed the codename {codename}** from "
"**{member.display_name}**"
msgstr ""

#: modules/codenames.py:148
#, python-brace-format
msgid " The codename **{codename}** belongs to **{member.display_name}**."
msgstr ""

#: modules/codenames.py:170
#, python-brace-format
msgid ""
"\n"
"\n"
"You can also be contacted through the secret codename(s) : {names_str}"
msgstr ""

#: modules/codenames.py:177
#, python-brace-format
msgid ""
"__Codenames management :mage::__\n"
"- `{self.bot.prefix}codename list` : list all codenames\n"
"- `{self.bot.prefix}codename list <player>` : list all the codenames of "
"`<player>`\n"
"- `{self.bot.prefix}codename info <codename>` : gives the player behind the "
"codename `<codename>`\n"
"- `{self.bot.prefix}codename add <player> <codename>` : attributes the "
"codename `<codename>` to `<player>`\n"
"- `{self.bot.prefix}codename delete <codename>` : removes the codename "
"`<codename>` (from whoever has it)."
msgstr ""

#: modules/codenames.py:178
msgid ""
"Codenames are case insensitive, but player names are case sensitive (e.g. "
"`Swordfish` and `swordfish` are the same codename, but `John doe` and `john "
"doe` can be two different players). Players can also be given by Discord UID."
msgstr ""

#: modules/messaging.py:22
msgid "Anonymous messaging and delayed messages"
msgstr ""

#: modules/messaging.py:34
msgid "[messaging] codenames module found, codenames integration activated."
msgstr ""

#: modules/messaging.py:37
msgid ""
"[messaging] codenames module not found, codenames will not be available for "
"anonymous messaging."
msgstr ""

#: modules/messaging.py:55
#, python-brace-format
msgid ""
":mailbox_with_mail: **You received the following message**, adressed to "
"\"{address}\":\n"
">>> {message}"
msgstr ""

#: modules/messaging.py:57
#, python-brace-format
msgid ""
":mailbox_with_mail: **You received the following message**, adressed to your "
"name ({recipient.display_name}):\n"
">>> {message}"
msgstr ""

#: modules/messaging.py:73
#, python-brace-format
msgid ":incoming_envelope: Your message was successfully sent to \"{who}\""
msgstr ""

#: modules/messaging.py:74
#, python-brace-format
msgid ""
":incoming_envelope: I **sent to \"{who}\"** (which is a codename for {target."
"display_name}) **the following message**:\n"
">>> {message}"
msgstr ""

#: modules/messaging.py:77
#, python-brace-format
msgid ""
":incoming_envelope: Your message was successfully sent to {target."
"display_name}"
msgstr ""

#: modules/messaging.py:78
#, python-brace-format
msgid ""
":incoming_envelope: I **sent to {target.display_name}** the following "
"message:\n"
">>> {message}"
msgstr ""

#: modules/messaging.py:80
#, python-brace-format
msgid ""
":envelope::no_entry_sign: Impossible to find recipient \"{who}\", this "
"should be a player or a codename. *(Player names are case sensitive)*"
msgstr ""

#: modules/messaging.py:81
#, python-brace-format
msgid ""
":envelope::no_entry_sign: I **tried to send a message to \"{who}\"**, but "
"**it did not work** because the bot could not find \"{who}\". The message "
"was:\n"
">>> {message}"
msgstr ""

#: modules/messaging.py:108
#, python-brace-format
msgid ""
":timer::incoming_envelope: The **scheduled message with ID `{msg_id}`) could "
"not be delivered !**"
msgstr ""

#: modules/messaging.py:115
#, python-brace-format
msgid ""
":timer::incoming_envelope: The **scheduled message to \"{address}\"** (which "
"is a codename for {recipient.display_name}), scheduled by {sender."
"display_name} for {sendtime_str} (message ID `{msg_id}`), **has been "
"delivered !** The message was:\n"
">>> {msg}"
msgstr ""

#: modules/messaging.py:117
#, python-brace-format
msgid ""
":timer::incoming_envelope: The **scheduled message to {recipient."
"display_name}**, scheduled by {sender.display_name} for {sendtime_str} "
"(message ID `{msg_id}`), **has been delivered !** The message was:\n"
">>> {msg}"
msgstr ""

#: modules/messaging.py:132
#, python-brace-format
msgid ""
"__Message ID `{msg_id}`:__ **sender:** {sender.display_name}, **recipient:** "
"\"{address}\" (aka {recipient.display_name}), **to be sent at:** "
"{sendtime_str}, content:\n"
">>> {msg}"
msgstr ""

#: modules/messaging.py:134
#, python-brace-format
msgid ""
"__Message ID `{msg_id}`:__ **sender:** {sender.display_name}, **recipient:** "
"{recipient.display_name}, **to be sent at:** {sendtime_str}, content:\n"
">>> {msg}"
msgstr ""

#: modules/messaging.py:170
#, python-brace-format
msgid ""
":timer::envelope::no_entry_sign: Delay must be higher than zero. (To send "
"instant messages, use `{self.bot.prefix}anonymous_message`.)"
msgstr ""

#: modules/messaging.py:176
#, python-brace-format
msgid ""
":timer::envelope::no_entry_sign: Impossible to find recipient \"{who}\", "
"this should be a player or a codename."
msgstr ""

#: modules/messaging.py:181 modules/messaging.py:186
#, python-brace-format
msgid ""
":timer::envelope: Message scheduled; message ID `{msg_id}`, will be "
"delivered at (about) {sendtime_str}."
msgstr ""

#: modules/messaging.py:182
#, python-brace-format
msgid ""
":timer::envelope: I **scheduled message to \"{who}\"** (which is a codename "
"for {target.display_name}) to be delivered at {sendtime_str} (message ID "
"`{msg_id}`). The message is:\n"
">>> {message}"
msgstr ""

#: modules/messaging.py:187
#, python-brace-format
msgid ""
":timer::envelope: I **scheduled message to {target.display_name}** to be "
"delivered at {sendtime_str} (message ID `{msg_id}`). The message is:\n"
">>> {message}"
msgstr ""

#: modules/messaging.py:198
msgid "No scheduled messages are pending for the current game."
msgstr ""

#: modules/messaging.py:201
#, python-brace-format
msgid "__{n} scheduled messages for the current game:__"
msgstr ""

#: modules/messaging.py:216
#, python-brace-format
msgid ":timer::envelope::x: Delayed message with id `{msgid}` canceled."
msgstr ""

#: modules/messaging.py:217
#, python-brace-format
msgid ""
":timer::envelope::x: I **canceled the delayed message** with ID `{msg_id}`."
msgstr ""

#: modules/messaging.py:219
#, python-brace-format
msgid "No message found with id `{msgid}` in the current game."
msgstr ""

#: modules/messaging.py:248
#, python-brace-format
msgid ""
"__Anonymous messaging:__\n"
"You can send **anonymous messages** with `{self.bot.prefix}anonymous_message "
"<recipient> <message>`. The recipient can be a player name or a codename. "
"Player names are case sensitive.\n"
"You can use \"double quotes\" to put spaces in the recipient name, for "
"example `{self.bot.prefix}anonymous_message \"John Smith\" What's up?`\n"
"If you want the recipient to answer you, be sure to sign the message! (Use a "
"codename for anonymity)"
msgstr ""

#: modules/messaging.py:255
#, python-brace-format
msgid ""
"__Delayed messages :mage::__\n"
"You can send **delayed anonymous messages** with `{self.bot.prefix}"
"delayed_message <recipient> <delay> <message>`. The delay is in minutes. The "
"command otherwise works similarly to `{self.bot.prefix}anonymous_message`.\n"
"You can also use the following management commands:\n"
"- `{self.bot.prefix}list_messages`: lists all pending delayed messages\n"
"- `{self.bot.prefix}cancel_message <msg_id>`: cancel a delayed m essage "
"identified by its message ID (given when created or by `{self.bot.prefix}"
"list_messages`)"
msgstr ""

#: modules/spying.py:21
msgid ""
"`degradation` must be a value of 1 or more. (If you want no degradation, "
"just don't pass any `degradation` parameter.)"
msgstr ""

#: modules/spying.py:63
msgid "Make the bot repeat to a user everything that happens in a channel."
msgstr ""

#: modules/spying.py:127
#, python-brace-format
msgid "Voice {index}"
msgstr ""

#: modules/spying.py:143
#, python-brace-format
msgid "**(:spy:) {authorlabel} :** {processedmessage}"
msgstr ""

#: modules/spying.py:154
#, python-brace-format
msgid ""
"Use `{self.bot.prefix}spying add <player> <channel> [hide_names|show_names] "
"[<degradation>]` to create a spying, `{self.bot.prefix}spying stop <player>` "
"to stop a spying, `{self.bot.prefix}spying list` to list all current spyings."
msgstr ""

#: modules/spying.py:162 modules/spying.py:210
msgid "names are hidden"
msgstr ""

#: modules/spying.py:162 modules/spying.py:210
msgid "names are visible"
msgstr ""

#: modules/spying.py:163
#, python-brace-format
msgid "**{spy.display_name}** is spying **{channel.name}** ({names_hidden})"
msgstr ""

#: modules/spying.py:175
msgid "There is currently no spying going on in this game."
msgstr ""

#: modules/spying.py:178
msgid "__Current spyings:__"
msgstr ""

#: modules/spying.py:191
#, python-brace-format
msgid "{channel} does not exist or is not a text channel"
msgstr ""

#: modules/spying.py:195 modules/spying.py:227
#, python-brace-format
msgid "Unknown player {player}"
msgstr ""

#: modules/spying.py:211
msgid "no degradation"
msgstr ""

#: modules/spying.py:211
#, python-brace-format
msgid "degradation: {degradation}"
msgstr ""

#: modules/spying.py:212 modules/spying.py:214
#, python-brace-format
msgid ""
":spy: **{spy.display_name}** is now spying **{spied_channel.name}** "
"({names_hidden}; {degradation_str})"
msgstr ""

#: modules/spying.py:213
msgid ":spy: **You are now spying a text channel**"
msgstr ""

#: modules/spying.py:216
#, python-brace-format
msgid ""
"User {spy.display_name} is already spying a channel. Only one channel can be "
"spied at a time."
msgstr ""

#: modules/spying.py:232
#, python-brace-format
msgid ":spy::no_entry_sign: **{spy.display_name}** is no longer spying."
msgstr ""

#: modules/spying.py:233
msgid ":spy: **You have stopped spying a text channel**"
msgstr ""

#: modules/spying.py:234
#, python-brace-format
msgid ":spy::no_entry_sign: **{spy.display_name}** is no longer spying.**"
msgstr ""

#: modules/spying.py:236
#, python-brace-format
msgid "User {spy.display_name} is not spying any channel."
msgstr ""

#: modules/spying.py:252
#, python-brace-format
msgid ""
"Degraded text exemple for degradation={degradation} (Markov process "
"probabilities p01={p01}, p10={p10}):\n"
">>> {degraded_message}."
msgstr ""

#: modules/spying.py:274
#, python-brace-format
msgid ""
"__Channel spying :mage::__\n"
"You can setup the bot to have it repeat all that happens in a channel to a "
"player, in private messages. This can be used for when a character is spying "
"on a scene without the participants knowing it. A character can only spy on "
"one channel at once (but a channel can have multiple spies). The commands "
"are:\n"
"- `{self.bot.prefix}spying add <player> <channel> [hide_names|show_names] "
"[<degradation>]`: have the player `<player>` spy on the channel "
"`<channel>`.\n"
"  * Passing the optional `hide_names` argument makes the names of the "
"participants unknown to the spy (they will be replaced by \"Voice 1\", "
"\"Voice 2\", etc.), this can be used e.g. if the spy can only listen but not "
"see, or does not recognize the participants. The default is to show names. \n"
"  * Passing an optional `<degradation>` numeric argument will randomly "
"degrade the copied text, replacing part of it with dots. You can test "
"degradation rates with the `$test_degradation` command (see below). The "
"default is no degradation."
msgstr ""

#: modules/spying.py:275
#, python-brace-format
msgid ""
"- `{self.bot.prefix}spying stop <player>`: stops the spying by `<player>`, "
"if any.\n"
"- `{self.bot.prefix}spying list`: lists all current spying in the game.\n"
"- `{self.bot.prefix}test_degradation <degradation> <test message>`: allows "
"you to test degradation rates: this will send you back `<test message>` "
"degraded as if it had been spied with `<degradation>` degradation rate. The "
"value must be at least 1. 1 is minimal degradation (just a character here "
"and there). 10 is about half the text being lost. Higher numbers lead to "
"heavy degradation. Keep in mind that degradation is random, so it may "
"randomly be milder or worse sometimes."
msgstr ""

#: modules/test.py:9
msgid "Test"
msgstr ""

#: modules/test.py:9
msgid "Test module for Panopticon v2"
msgstr ""

#: modules/test.py:14
msgid "Test message !"
msgstr ""

#: modules/test.py:20
msgid "Test extension setup."
msgstr ""

#: modules/test.py:24
#, python-brace-format
msgid ""
"Hello from test extension {ctx.author.display_name}. Here is the test "
"message: {self.my_message}"
msgstr ""

#: modules/test.py:27
msgid ""
"__Test extension:__\n"
"This is shown to non-GM users."
msgstr ""

#: modules/test.py:30
msgid ""
"__Test extension:__\n"
"This is shown to GM users :mage:."
msgstr ""

#: modules/tietextvoice.py:20
msgid "Allows to tie a text channel visibility to presence in a voice channel."
msgstr ""

#: modules/tietextvoice.py:128
#, python-brace-format
msgid ""
"Use `{self.bot.prefix}voicelink add <voice channel> <text channel>` to link "
"a text channel to a voice channel, `{self.bot.prefix}voicelink del <text "
"channel>` to remove any link affecting a text channel, and `{self.bot.prefix}"
"voicelink list` to list all existing links."
msgstr ""

#: modules/tietextvoice.py:145
#, python-brace-format
msgid "'{voicechan}' does not exist or is not a voice channel"
msgstr ""

#: modules/tietextvoice.py:148 modules/tietextvoice.py:169
#, python-brace-format
msgid "{textchan} does not exist or is not a text channel"
msgstr ""

#: modules/tietextvoice.py:155
#, python-brace-format
msgid ""
"{textchan} is already linked to the voice channel '{existing_voicechannel."
"name}'"
msgstr ""

#: modules/tietextvoice.py:157
#, python-brace-format
msgid "{textchan} is now linked to '{voicechan}'"
msgstr ""

#: modules/tietextvoice.py:158
#, python-brace-format
msgid ""
":pencil::link::speaker: I **linked the text channel {textchan} to the voice "
"channel '{voicechan}'**. {textchan} is now visible to players in {voicechan}"
msgstr ""

#: modules/tietextvoice.py:174
#, python-brace-format
msgid "Link destroyed; {textchan} is not linked to a voice channel any more."
msgstr ""

#: modules/tietextvoice.py:175
#, python-brace-format
msgid ""
":pencil::link::speaker::no_entry_sign: I **unlinked the text channel "
"{textchan} from its voice channel**. Previously authorized players will be "
"removed."
msgstr ""

#: modules/tietextvoice.py:177
#, python-brace-format
msgid "{textchan} is not linked to any voice channel."
msgstr ""

#: modules/tietextvoice.py:187 modules/tietextvoice.py:207
msgid "No voice/text links in the current game."
msgstr ""

#: modules/tietextvoice.py:190
#, python-brace-format
msgid "__{n_links} text channel(s) linked to voice channels:__"
msgstr ""

#: modules/tietextvoice.py:195
#, python-brace-format
msgid "- **{text_channel}** is linked to **{voice_channel}**"
msgstr ""

#: modules/tietextvoice.py:210
#, python-brace-format
msgid ""
"{n_links} text channel(s) linked to voice channels in the current game, "
"resyncing permissions..."
msgstr ""

#: modules/tietextvoice.py:218
#, python-brace-format
msgid ""
":pencil::link::speaker::arrows_counterclockwise: Resynced membership for "
"text channels {channelsstr}."
msgstr ""

#: modules/tietextvoice.py:219
#, python-brace-format
msgid ""
":pencil::link::speaker::arrows_counterclockwise: I **resynced membership for "
"all linked text channels** ({channelsstr})"
msgstr ""

#: modules/tietextvoice.py:223
#, python-brace-format
msgid "{textchannel} does not exist or is not a text channel"
msgstr ""

#: modules/tietextvoice.py:227
#, python-brace-format
msgid "{textchannel} is not linked to any voice channel."
msgstr ""

#: modules/tietextvoice.py:231
#, python-brace-format
msgid ""
":pencil::link::speaker::arrows_counterclockwise: Resynced membership for "
"{text_channel.name} based on {voice_channel.name}."
msgstr ""

#: modules/tietextvoice.py:232
#, python-brace-format
msgid ""
":pencil::link::speaker::arrows_counterclockwise: I **resynced membership** "
"for the text channel **{text_channel.name} linked to {voice_channel.name}**"
msgstr ""

#: modules/tietextvoice.py:251
#, python-brace-format
msgid ""
"__Voice/text channels linking management :mage::__\n"
"- `{self.bot.prefix}voicelink add <voice channel> <text channel>` : links "
"`<text channel>` to `<voice channel>` (the text channel is visible only to "
"the members in the voice channel)\n"
"- `{self.bot.prefix}voicelink del <text channel>` : unlink `<text channel>` "
"from its linked voice channel\n"
"- `{self.bot.prefix}voicelink list` : list all existing links.\n"
"- `{self.bot.prefix}voicelink resync [<text channel>|all]` : resynchronize "
"the permissions for the given text channel (or for all currently linked "
"channels) based on the linked voice channel (use in case of glitches). This "
"will briefly remove and readd everyone to the channel."
msgstr ""

#: modules/tietextvoice.py:252
msgid ""
"A text channel can only be linked to one voice channel, but multiple text "
"channels can be linked to the same voice channel. When a link is created, "
"the bot will turn off visibility for `@everyone` if needed, and add the "
"people in the voice channel. When a link is destroyed, it will just remove "
"everyone and keep the channel private.\n"
"*Please do not manually set/delete permissions for individual members for a "
"text channel with an active link, or they will be destroyed by the bot.* The "
"bot will not touch permissions for roles one way or another, so if you want "
"to give some people permanent access to the text channel or add/deny "
"specific permissions, give them a role and give the role permissions on the "
"channel."
msgstr ""
