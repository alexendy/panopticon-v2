FROM gorialis/discord.py

RUN mkdir -p /usr/src

WORKDIR /usr/src

RUN git clone https://gitlab.com/mirandablue/panopticon-v2

WORKDIR /usr/src/panopticon-v2

RUN pip3 install .

CMD [ "python3", "start.py" ]
