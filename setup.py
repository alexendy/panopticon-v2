from setuptools import setup, find_packages

setup(name='panopticon2',
      install_requires=['discord.py','aiofiles','pymongo','motor'],
      version='0.3.3',
      packages=find_packages(),
      include_package_data=True,
      author='Miranda Coninx',
      author_email='miranda@roboctopus.org'
)
