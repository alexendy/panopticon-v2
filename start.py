from panopticon2 import start

import json

with open("config.json",'r') as fd:
    config = json.load(fd)


start(config)
