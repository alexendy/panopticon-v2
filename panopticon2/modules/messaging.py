import discord
import typing
import asyncio

from bson.objectid import ObjectId
from bson.errors import InvalidId

from discord.ext import commands, tasks
from datetime import date, datetime, timedelta

from panopticon2.utils import *
from panopticon2.checks import has_gm_rights, GMRightsNeeded

_ = str

messages_sending_task_period=1





class MessagingCog(commands.Cog, name="Messaging", description=_("Anonymous messaging and delayed messages")):
    def __init__(self, bot):
        global _
        self.bot = bot
        self.codenames_module = None
        self.bot.loop.create_task(self.setup()) # For setup that needs an initialized bot
    
    
    async def setup(self):
        global _
        await self.bot.wait_until_ready()
        if "codenames" in self.bot.loaded_modules:
            print(_("[messaging] codenames module found, codenames integration activated."))
            self.codenames_module = self.bot.get_cog("Codenames")
        else:
            print(_("[messaging] codenames module not found, codenames will not be available for anonymous messaging."))
        self.bot.add_help(self.help_regular, self.help_gm)
        self.send_due_messages.start()
    
    async def get_by_name_or_codename(self, gid:int, identifier:str) -> (discord.Member, bool): # Second argument is True if identifier was a codename
        if self.codenames_module: # If we have a codenames module, start by that
            uid = await self.codenames_module.get_user_by_codename(gid, identifier)
            if uid: # Found !
                member = self.bot.get_member(gid, uid) # Get the member object
                return (member, True)
        # No codename found, or codename unavailable
        member = self.bot.get_member(gid, identifier)
        return (member, False)
    
    # Lower level function used by both instant and delayed message
    async def deliver_message(self, gid:int, recipient:discord.Member, message:str, address:typing.Optional[str]=None):
        _ = await self.bot.get_current_translation_gid(gid)
        if address:
            await recipient.send(f(_(":mailbox_with_mail: **You received the following message**, adressed to \"{address}\":\n>>> {message}")))
        else:
            await recipient.send(f(_(":mailbox_with_mail: **You received the following message**, adressed to your name ({recipient.display_name}):\n>>> {message}")))
    
    
    # Main anonymous messaging function
    
    @commands.command(aliases=["message_anonyme","message"])
    @commands.dm_only()
    async def anonymous_message(self, ctx, who:str, *, message:str):
        message = message.strip("\"'")
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        target, is_codename = await self.get_by_name_or_codename(gid, who)
        sender = self.bot.get_member(gid, ctx.author.id) # Get the server display name (a Member and not a User)
        if target: # We have a valid recipient !
            if is_codename:
                await self.deliver_message(gid, target, message, who)
                await ctx.send(f(_(":incoming_envelope: Your message was successfully sent to \"{who}\"")))
                await self.bot.log(gid, f(_(":incoming_envelope: I **sent to \"{who}\"** (which is a codename for {target.display_name}) **the following message**:\n>>> {message}")), sender)
            else:
                await self.deliver_message(gid, target, message, None)
                await ctx.send(f(_(":incoming_envelope: Your message was successfully sent to {target.display_name}")))
                await self.bot.log(gid, f(_(":incoming_envelope: I **sent to {target.display_name}** the following message:\n>>> {message}")), sender)
        else: # Invalid recipient
                await ctx.send(f(_(":envelope::no_entry_sign: Impossible to find recipient \"{who}\", this should be a player or a codename. *(Player names are case sensitive)*")))
                await self.bot.log(gid, f(_(":envelope::no_entry_sign: I **tried to send a message to \"{who}\"**, but **it did not work** because the bot could not find \"{who}\". The message was:\n>>> {message}")), sender)

    # Delayed messaging
    
    # Message creation coroutine
    async def schedule_message(self, gid:int, sender_uid:int, rcpt_uid:int, msg:str, delay:float, address:typing.Optional[str]=None):
        send_time = datetime.now() + timedelta(minutes=delay)
        message = {"gid": gid,
                    "sender_uid": sender_uid,
                    "rcpt_uid": rcpt_uid,
                    "msg": msg,
                    "send_time": send_time.timestamp(),
                    "address": address}
        result = await self.bot.mongo["master_index"].delayed_messages.insert_one(message)
        return (send_time, result.inserted_id)
    
    # Delivers a message
    async def send_delayed_message(self, message:dict):
        gid = message["gid"]
        _ = await self.bot.get_current_translation_gid(gid)
        sender = self.bot.get_member(gid, message["sender_uid"])
        recipient = self.bot.get_member(gid, message["rcpt_uid"])
        msg = message["msg"]
        address = message["address"] # Can be None
        sendtime = datetime.fromtimestamp(message["send_time"])
        msg_id = message["_id"]
        if not recipient:
            await self.bot.log(gid, f(_(":timer::incoming_envelope: The **scheduled message with ID `{msg_id}`) could not be delivered !**")))
            return False
        # Send message
        await self.deliver_message(gid, recipient, msg, address)
        # Log, log, log !
        sendtime_str = str(sendtime)
        if address:
            await self.bot.log(gid, f(_(":timer::incoming_envelope: The **scheduled message to \"{address}\"** (which is a codename for {recipient.display_name}), scheduled by {sender.display_name} for {sendtime_str} (message ID `{msg_id}`), **has been delivered !** The message was:\n>>> {msg}")))
        else:
            await self.bot.log(gid, f(_(":timer::incoming_envelope: The **scheduled message to {recipient.display_name}**, scheduled by {sender.display_name} for {sendtime_str} (message ID `{msg_id}`), **has been delivered !** The message was:\n>>> {msg}")))
        return True
    
    # Format a message for printing
    async def format_message(self, message:dict):
        gid = message["gid"]
        _ = await self.bot.get_current_translation_gid(gid)
        sender = self.bot.get_member(gid, message["sender_uid"])
        recipient = self.bot.get_member(gid, message["rcpt_uid"])
        msg = message["msg"]
        address = message["address"] # Can be None
        sendtime = datetime.fromtimestamp(message["send_time"])
        msg_id = message["_id"]
        sendtime_str = str(sendtime)
        if address:
            return f(_("__Message ID `{msg_id}`:__ **sender:** {sender.display_name}, **recipient:** \"{address}\" (aka {recipient.display_name}), **to be sent at:** {sendtime_str}, content:\n>>> {msg}"))
        else:
            return f(_("__Message ID `{msg_id}`:__ **sender:** {sender.display_name}, **recipient:** {recipient.display_name}, **to be sent at:** {sendtime_str}, content:\n>>> {msg}"))
    
    # Get all scheduled messages for the game gid
    async def get_pending_messages(self, gid:int):
        messages = [message async for message in self.bot.mongo["master_index"].delayed_messages.find({'gid': gid}).sort('send_time')]
        return messages
    
    async def delete_delayed_message(self, gid:int, msg_id:str):
        try:
            result = await self.bot.mongo["master_index"].delayed_messages.delete_one({'gid': gid, '_id':ObjectId(msg_id)})
        except InvalidId:
            return False # Invalid ObjectID
        return (result.deleted_count > 0) # True if we did delete something
    
    async def reschedule_delayed_message(self, gid:int, msg_id:str, delta:float):
        try:
            message = await self.bot.mongo["master_index"].delayed_messages.find({'gid': gid, '_id':ObjectId(msg_id)})
        except InvalidId:
            return None
        if not message:
            return None
        sendtime = datetime.fromtimestamp(message["send_time"])
        sendtime += timedelta(minutes=delta)
        await self.bot.mongo["master_index"].delayed_messages.update_one({'_id': {"$eq" : message["_id"]}}, {'$set':{'send_time': sendtime.timestamp()}})
        return sendtime
    
    # Commands
    @commands.command(aliases=["scheduled_message","message_retarde","message_retardé","message_programme","message_programmé"])
    @commands.dm_only()
    async def delayed_message(self, ctx, who:str, delay:float, *, message:str):
        message = message.strip("\"'")
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        if delay <= 0:
            await ctx.send(f(_(":timer::envelope::no_entry_sign: Delay must be higher than zero. (To send instant messages, use `{self.bot.prefix}anonymous_message`.)")))
            return
        # Resolve recipient
        target, is_codename = await self.get_by_name_or_codename(gid, who)
        sender = self.bot.get_member(gid, ctx.author.id) # Get the server display name (a Member and not a User)
        if not target:
            await ctx.send(f(_(":timer::envelope::no_entry_sign: Impossible to find recipient \"{who}\", this should be a player or a codename.")))
            return
        if is_codename:
            send_time, msg_id = await self.schedule_message(gid, sender.id, target.id, message, delay, who)
            sendtime_str = str(send_time)
            await ctx.send(f(_(":timer::envelope: Message scheduled; message ID `{msg_id}`, will be delivered at (about) {sendtime_str}.")))
            await self.bot.log(gid, f(_(":timer::envelope: I **scheduled message to \"{who}\"** (which is a codename for {target.display_name}) to be delivered at {sendtime_str} (message ID `{msg_id}`). The message is:\n>>> {message}")), sender)
        else:
            send_time, msg_id = await self.schedule_message(gid, sender.id, target.id, message, delay, None)
            sendtime_str = str(send_time)
            await ctx.send(f(_(":timer::envelope: Message scheduled; message ID `{msg_id}`, will be delivered at (about) {sendtime_str}.")))
            await self.bot.log(gid, f(_(":timer::envelope: I **scheduled message to {target.display_name}** to be delivered at {sendtime_str} (message ID `{msg_id}`). The message is:\n>>> {message}")), sender)

    # Commands
    @commands.command(aliases=["liste_messages","lister_messages"])
    async def list_messages(self, ctx):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        messages = await self.get_pending_messages(gid)
        if not messages:
            await ctx.send(f(_("No scheduled messages are pending for the current game.")))
        else:
            n = len(messages)
            await ctx.send(f(_("__{n} scheduled messages for the current game:__")))
            for m in messages:
                msg_formatted = await self.format_message(m)
                await ctx.send(msg_formatted)
                await asyncio.sleep(0.2)

    # Commands
    @commands.command(aliases=["annuler_message"])
    async def cancel_message(self, ctx, msgid:str):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        good = await self.delete_delayed_message(gid, msgid)
        if good:
            await ctx.send(f(_(":timer::envelope::x: Delayed message with id `{msgid}` canceled.")))
            await self.bot.log(gid, f(_(":timer::envelope::x: I **canceled the delayed message** with ID `{msg_id}`.")), sender)
        else:
            await ctx.send(f(_("No message found with id `{msgid}` in the current game.")))
    
    # Message sending task
    @tasks.loop(minutes=messages_sending_task_period)
    async def send_due_messages(self):
        # Freeze time
        current_ts = datetime.now().timestamp()
        # Get due messages
        async for message in self.bot.mongo["master_index"].delayed_messages.find({'send_time': {"$lt" : current_ts}}).sort('send_time'):
            await self.send_delayed_message(message)
        # Delete all sent messages
        await self.bot.mongo["master_index"].delayed_messages.delete_many({'send_time': {"$lt" : current_ts}})
    
    @anonymous_message.error
    @delayed_message.error
    @list_messages.error
    @cancel_message.error
    async def error_messaging(self, ctx, error):
        _ = await self.bot.get_current_translation_ctx(ctx)
        if isinstance(error, GMRightsNeeded):
            await ctx.send(f(_("You need gamemaster rights to do this")))
        else:
            await ctx.send(f(_("Unknown error : {error}")))
            raise error

    
    async def help_regular(self, ctx):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        await ctx.send(f(_("__Anonymous messaging:__\nYou can send **anonymous messages** with `{self.bot.prefix}anonymous_message <recipient> <message>`. The recipient can be a player name or a codename. Player names are case sensitive.\nYou can use \"double quotes\" to put spaces in the recipient name, for example `{self.bot.prefix}anonymous_message \"John Smith\" What's up?`\nIf you want the recipient to answer you, be sure to sign the message! (Use a codename for anonymity)")))
        

    async def help_gm(self, ctx):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        await self.help_regular(ctx)
        await ctx.send(f(_("__Delayed messages :mage::__\nYou can send **delayed anonymous messages** with `{self.bot.prefix}delayed_message <recipient> <delay> <message>`. The delay is in minutes. The command otherwise works similarly to `{self.bot.prefix}anonymous_message`.\nYou can also use the following management commands:\n- `{self.bot.prefix}list_messages`: lists all pending delayed messages\n- `{self.bot.prefix}cancel_message <msg_id>`: cancel a delayed m essage identified by its message ID (given when created or by `{self.bot.prefix}list_messages`)")))


async def setup(bot):
    global _
    _ = bot.themes.get_default_translator() # Get system/default translator
    await bot.add_cog(MessagingCog(bot))
