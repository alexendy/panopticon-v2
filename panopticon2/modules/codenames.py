import discord
import typing
from discord.ext import commands, tasks

from panopticon2.utils import *
from panopticon2.checks import has_gm_rights, GMRightsNeeded

_ = str


pagination_increment = 10

class CodenamesCog(commands.Cog, name="Codenames", description=_("Codenames for anonymous messaging or other operations")):
    def __init__(self, bot):
        global _
        self.bot = bot
        self.bot.loop.create_task(self.setup()) # For setup that needs an initialized bot
    
    
    async def setup(self):
        await self.bot.wait_until_ready()
        self.bot.add_help(self.help_regular, self.help_gm)
        


    # Codename management low level functions
    # Get all codenames for a player and a game
    async def get_codenames_for_user(self, gid:int, uid:int):
        codenames = [r["codename"].capitalize() async for r in self.bot.mongo["server_"+str(gid)].codenames.find({'uid': uid})]
        return codenames

    # Get user ID for a codename (None if unknown codename)
    async def get_user_by_codename(self, gid:int, codename:str):
        record = await self.bot.mongo["server_"+str(gid)].codenames.find_one({'codename': codename.lower().strip()})
        if record:
            return record["uid"]
        else:
            return None
    # Create a codename. Return a bool error code
    async def create_codename(self, gid:int, uid:int, codename:str) -> bool: # get user ID for a codename (None if unknown)
        if not codename or await self.get_user_by_codename(gid,codename) is not None:
            return False # The codename already exists or is empty
        await self.bot.mongo["server_"+str(gid)].codenames.insert_one({'uid':uid, 'codename': codename.lower().strip()})
        return True

    # Delere a codename. Return a bool error code
    async def delete_codename(self, gid:int, codename:str) -> bool: # get user ID for a codename (None if unknown)
        result = await self.bot.mongo["server_"+str(gid)].codenames.delete_one({'codename': codename.lower().strip()})
        return (result.deleted_count > 0) # True if we did delete something

    # Get the full codename:uid dict
    async def list_all_codenames(self, gid:int) -> dict[str,int]:
        out = dict()
        async for record in self.bot.mongo["server_"+str(gid)].codenames.find():
            out[record["codename"].capitalize()] = record["uid"]
        return out
    
    
    
    @commands.group(invoke_without_command=True, aliases=["codenames"])
    async def codename(self, ctx):
        _ = await self.bot.get_current_translation_ctx(ctx)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        await ctx.send(f(_("Use `{self.bot.prefix}codename list` to list all codenames, `{self.bot.prefix}codename add <player> <codename>` to create a new codename, `{self.bot.prefix}codename delete <codename>` to remove a codename, or `{self.bot.prefix}codename info <codename>` to know who has a codename.")))


    @codename.command(aliases=["liste"])
    async def list(self, ctx, who:typing.Optional[str]):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        if who: # We asked for someone's codenames
            member = self.bot.get_member(gid, who)
            if not member:
                await ctx.send(f(_("Unknown member {who}")))
                return
            names = await self.get_codenames_for_user(gid, member.id)
            if names:
                names_str = ", ".join([f"**{n}**" for n in names])
                await ctx.send(f(_("The member **{who}** has codename(s) : {names_str}")))
            else:
                await ctx.send(f(_("The member **{who}** has no codename.")))
        else: # We asked for the complete list
            allnames = await self.list_all_codenames(gid)
            if len(allnames) == 0:
                await ctx.send(f(_("There are no codenames for anyone in this game.")))
                return
            sortedcodes = list(allnames.keys())
            sortedcodes.sort()
            entries = []
            for k in sortedcodes:
                member = self.bot.get_member(gid, allnames[k])
                entries.append(f"- {k} : {member.display_name}")
            # Paginate
            await ctx.send(f(_("Codenames for this game :")))
            for page in range(0,len(entries),pagination_increment):
                chunk = "\n".join(entries[page:page+pagination_increment])
            await ctx.send(chunk)
            

    @codename.command(aliases=["create","ajouter"])
    async def add(self, ctx, who:str, codename:str):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        member = self.bot.get_member(gid, who)
        if not member:
            await ctx.send(f(_("Unknown member {who}")))
            return
        if await self.create_codename(gid, member.id, codename):

            await member.send(f(_(":pen_fountain: You can now be contacted through the **secret codename : {codename}**")))
            await ctx.send(f(_(":pen_fountain: The codename {codename} has been attributed to {member.display_name}. They have been notified.")))
            await self.bot.log(gid, f(_(":pen_fountain: I **added the codename {codename}** to **{member.display_name}**")), ctx.author)
        else:
            await ctx.send(f(_("This codename is already used or invalid.")))

    @codename.command(aliases=["del", "remove", "enlever", "supprimer"])
    async def delete(self, ctx, codename:str):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        owner_id = await self.get_user_by_codename(gid, codename)
        if not owner_id:
            await ctx.send(f(_("Codename {codename} does not exist.")))
            return
        await self.delete_codename(gid, codename)
        member = self.bot.get_member(gid, owner_id)
        await member.send(f(_(":pen_fountain::no_entry_sign: You **can no longer be contacted** through the secret codename : {codename}")))
        await ctx.send(f(_(":pen_fountain::no_entry_sign: The codename {codename} has been removed from {member.display_name}. They have been notified.")))
        await self.bot.log(gid, f(_(":pen_fountain::no_entry_sign: I **removed the codename {codename}** from **{member.display_name}**")), ctx.author)

    @codename.command(aliases=["who"])
    async def info(self, ctx, codename:str):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        owner_id = await self.get_user_by_codename(gid, codename)
        if not owner_id:
            await ctx.send(f(_("Codename {codename} does not exist.")))
            return
        member = self.bot.get_member(gid, owner_id)
        await ctx.send(f(_(" The codename **{codename}** belongs to **{member.display_name}**.")))

    @codename.error
    @list.error
    @add.error
    @delete.error
    @info.error
    async def error_codename(self, ctx, error):
        _ = await self.bot.get_current_translation_ctx(ctx)
        if isinstance(error, GMRightsNeeded):
            await ctx.send(f(_("You need gamemaster rights to do this")))
        else:
            await ctx.send(f(_("Unknown error : {error}")))
            raise error

    
    async def help_regular(self, ctx):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        names = await self.get_codenames_for_user(gid, ctx.author.id)
        if names:
            names_str = ", ".join([f"**{n}**" for n in names])
            await ctx.send(f(_("\n\nYou can also be contacted through the secret codename(s) : {names_str}")))
        

    async def help_gm(self, ctx):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        await self.help_regular(ctx) # Summon regular help
        await ctx.send(f(_("__Codenames management :mage::__\n- `{self.bot.prefix}codename list` : list all codenames\n- `{self.bot.prefix}codename list <player>` : list all the codenames of `<player>`\n- `{self.bot.prefix}codename info <codename>` : gives the player behind the codename `<codename>`\n- `{self.bot.prefix}codename add <player> <codename>` : attributes the codename `<codename>` to `<player>`\n- `{self.bot.prefix}codename delete <codename>` : removes the codename `<codename>` (from whoever has it).")))
        await ctx.send(_("Codenames are case insensitive, but player names are case sensitive (e.g. `Swordfish` and `swordfish` are the same codename, but `John doe` and `john doe` can be two different players). Players can also be given by Discord UID."))


async def setup(bot):
    global _
    _ = bot.themes.get_default_translator() # Get system/default translator
    await bot.add_cog(CodenamesCog(bot))
