import discord
import typing
from discord.ext import commands, tasks

from panopticon2.utils import *
from panopticon2.checks import has_gm_rights, GMRightsNeeded

_ = str


pagination_increment = 10

class AlreadyLinkedError(RuntimeError):
    def __init__(self, gid, text_id, voice_id):
        self.gid = gid
        self.text_id = text_id
        self.voice_id = voice_id


class TieTextVoiceCog(commands.Cog, name="TieTextVoice", description=_("Allows to tie a text channel visibility to presence in a voice channel.")):
    def __init__(self, bot):
        global _
        self.bot = bot
        self.bot.loop.create_task(self.setup()) # For setup that needs an initialized bot
    
    
    async def setup(self):
        await self.bot.wait_until_ready()
        self.bot.add_help(None, self.help_gm) # GM only commands
        


#     (member, before, after)

        # Error handling
    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        # We want to react to people joining, leaving or switching channels, which means the channel before != channel after
        # if before.channel == after.channel, then it is another event (e.g. mute) we are not interested in
        if member.bot:
            return # Ignore bots
        if before.channel == after.channel:
            return
        guild = member.guild
        leaving = before.channel
        joining = after.channel
        if leaving: #If it is None, we are not leaving any channel
            linked_channels_ids = await self.get_linked_textchannels(guild.id, leaving.id)
            # If the voice channel is not linked to anything, this will just return []
            for channel_id in linked_channels_ids:
                textchannel = guild.get_channel(channel_id)
                await self.remove_from_text_channel(textchannel, member)
        if joining: #If it is None, we are not joining any channel
            linked_channels_ids = await self.get_linked_textchannels(guild.id, joining.id)
            # If the voice channel is not linked to anything, this will just return []
            for channel_id in linked_channels_ids:
                textchannel = guild.get_channel(channel_id)
                await self.add_to_text_channel(textchannel, member)
            

    # Lower level channel management functions
    async def remove_from_text_channel(self, textchannel:discord.TextChannel, member:discord.Member):
        # Remove member-specific permissions
        # @everyone had view_channel set to False, so this should be enough except if the member has access through a role
        await textchannel.set_permissions(member, overwrite=None)
        
    async def add_to_text_channel(self, textchannel:discord.TextChannel, member:discord.Member):
        # Give member access
        await textchannel.set_permissions(member, view_channel=True)
    
    
    
    async def clear_all_member_overwrites(self, channel:discord.abc.GuildChannel):
        overwrites = channel.overwrites
        for permission_holder in overwrites.keys():
            if isinstance(permission_holder, discord.Role) or permission_holder.bot:
                continue # Ignore roles and bots
            # This is a non-bot member
            await channel.set_permissions(permission_holder, overwrite=None) # Destroy the overwrite
            

    async def initial_channel_setup(self, textchannel:discord.TextChannel, voicechannel:discord.VoiceChannel):
        everyone = textchannel.guild.default_role
        # Make channel private
        await textchannel.set_permissions(everyone, view_channel=False)
        # Remove any manually set individual permissions that may be around
        await self.clear_all_member_overwrites(textchannel)
        # Add everyone currently in the voice channel
        for member in voicechannel.members:
            if not member.bot: # Ignore bots
                await self.add_to_text_channel(textchannel, member)


    #Low level functions to handle the database
    async def add_channel_link(self, gid:int, voice_id:int, text_id:int):
        existing = await self.bot.mongo["server_"+str(gid)].textvoicelinks.find_one({"gid":gid, "text_id":text_id})
        if existing:
            raise AlreadyLinkedError(gid, text_id, existing["voice_id"])
        await self.bot.mongo["server_"+str(gid)].textvoicelinks.insert_one({"gid":gid, "voice_id":voice_id, "text_id":text_id})
        #TODO (or in calling function) : update current status

    async def del_channel_link(self, gid:int, text_id:int):
        result = await self.bot.mongo["server_"+str(gid)].textvoicelinks.delete_one({"gid":gid, "text_id":text_id})
        return (result.deleted_count > 0) # True if we did delete something
        #TODO (or in calling function) : update current status

    async def get_channel_links(self, gid:int):
        return [(r["text_id"],r["voice_id"]) async for r in self.bot.mongo["server_"+str(gid)].textvoicelinks.find({'gid': gid})]
    
    async def get_linked_textchannels(self, gid:int, voice_id:int):
        return [r["text_id"] async for r in self.bot.mongo["server_"+str(gid)].textvoicelinks.find({'gid': gid, 'voice_id':voice_id})]

    async def get_linked_voicechannel(self, gid:int, text_id:int):
        record = await self.bot.mongo["server_"+str(gid)].textvoicelinks.find_one({'gid': gid, 'text_id':text_id})
        if not record:
            return None
        else:
            return record["voice_id"]


    
    
    @commands.group(invoke_without_command=True, aliases=["lienvocal","lien_vocal","lien"])
    async def voicelink(self, ctx):
        _ = await self.bot.get_current_translation_ctx(ctx)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        await ctx.send(f(_("Use `{self.bot.prefix}voicelink add <voice channel> <text channel>` to link a text channel to a voice channel, `{self.bot.prefix}voicelink del <text channel>` to remove any link affecting a text channel, and `{self.bot.prefix}voicelink list` to list all existing links.")))






    @voicelink.command(aliases=["create","ajouter","creer","créer"])
    async def add(self, ctx, voicechan:str, textchan:str):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        guild = self.bot.get_guild(gid)
        voice_channel = find_channel_type(guild, voicechan, discord.VoiceChannel)
        text_channel = find_channel_type(guild, textchan, discord.TextChannel)
        if not voice_channel:
            await ctx.send(f(_("'{voicechan}' does not exist or is not a voice channel")))
            return
        if not text_channel:
            await ctx.send(f(_("{textchan} does not exist or is not a text channel")))
            return
        try:
            await self.add_channel_link(gid, voice_channel.id, text_channel.id) # Add to DB
            await self.initial_channel_setup(text_channel, voice_channel) # Setup initial permissions
        except AlreadyLinkedError as err:
            existing_voicechannel = guild.get_channel(err.voice_id)
            await ctx.send(f(_("{textchan} is already linked to the voice channel '{existing_voicechannel.name}'")))
            return
        await ctx.send(f(_("{textchan} is now linked to '{voicechan}'")))
        await self.bot.log(gid,f(_(":pencil::link::speaker: I **linked the text channel {textchan} to the voice channel '{voicechan}'**. {textchan} is now visible to players in {voicechan}")), ctx.author)
    
    @voicelink.command(aliases=["del","remove","enlever","supprimer"])
    async def delete(self, ctx, textchan:str):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        guild = self.bot.get_guild(gid)
        text_channel = find_channel_type(guild, textchan, discord.TextChannel)
        if not text_channel:
            await ctx.send(f(_("{textchan} does not exist or is not a text channel")))
            return
        done = await self.del_channel_link(gid, text_channel.id)
        if done:
            await self.clear_all_member_overwrites(text_channel) # Cleanup
            await ctx.send(f(_("Link destroyed; {textchan} is not linked to a voice channel any more.")))
            await self.bot.log(gid,f(_(":pencil::link::speaker::no_entry_sign: I **unlinked the text channel {textchan} from its voice channel**. Previously authorized players will be removed.")), ctx.author)
        else:
            await ctx.send(f(_("{textchan} is not linked to any voice channel.")))
        
    @voicelink.command(aliases=["liste"])
    async def list(self, ctx):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        links = await self.get_channel_links(gid)
        if not links:
            await ctx.send(f(_("No voice/text links in the current game.")))
        else:
            n_links = len(links)
            await ctx.send(f(_("__{n_links} text channel(s) linked to voice channels:__")))
            guild = self.bot.get_guild(gid)
            for (textid, voiceid) in links:
                text_channel = guild.get_channel(textid)
                voice_channel = guild.get_channel(voiceid)
                await ctx.send(f(_("- **{text_channel}** is linked to **{voice_channel}**")))
    
    @voicelink.command(aliases=["sync"])
    async def resync(self, ctx, textchannel:str):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        guild = self.bot.get_guild(gid)
        if textchannel == "all":
            links = await self.get_channel_links(gid)
            if not links:
                await ctx.send(f(_("No voice/text links in the current game.")))
            else:
                n_links = len(links)
                await ctx.send(f(_("{n_links} text channel(s) linked to voice channels in the current game, resyncing permissions...")))
                list_channels = []
                for (textid, voiceid) in links:
                    text_channel = guild.get_channel(textid)
                    voice_channel = guild.get_channel(voiceid)
                    list_channels.append(text_channel.name)
                    await self.initial_channel_setup(text_channel, voice_channel)
                channelsstr = ", ".join(list_channels)
                await ctx.send(f(_(":pencil::link::speaker::arrows_counterclockwise: Resynced membership for text channels {channelsstr}.")))
                await self.bot.log(gid,f(_(":pencil::link::speaker::arrows_counterclockwise: I **resynced membership for all linked text channels** ({channelsstr})")), ctx.author)
        else:
            text_channel = find_channel_type(guild, textchannel, discord.TextChannel)
            if not text_channel:
                await ctx.send(f(_("{textchannel} does not exist or is not a text channel")))
                return
            linked_voice_channel_id = await self.get_linked_voicechannel(gid, text_channel.id)
            if not linked_voice_channel_id:
                await ctx.send(f(_("{textchannel} is not linked to any voice channel.")))
                return
            voice_channel = guild.get_channel(linked_voice_channel_id)
            await self.initial_channel_setup(text_channel, voice_channel)
            await ctx.send(f(_(":pencil::link::speaker::arrows_counterclockwise: Resynced membership for {text_channel.name} based on {voice_channel.name}.")))
            await self.bot.log(gid,f(_(":pencil::link::speaker::arrows_counterclockwise: I **resynced membership** for the text channel **{text_channel.name} linked to {voice_channel.name}**")), ctx.author)

    
    @voicelink.error
    @add.error
    @delete.error
    @list.error
    @resync.error
    async def error_tievoice(self, ctx, error):
        _ = await self.bot.get_current_translation_ctx(ctx)
        if isinstance(error, GMRightsNeeded):
            await ctx.send(f(_("You need gamemaster rights to do this")))
        else:
            await ctx.send(f(_("Unknown error : {error}")))
            raise error

    async def help_gm(self, ctx):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        await ctx.send(f(_("__Voice/text channels linking management :mage::__\n- `{self.bot.prefix}voicelink add <voice channel> <text channel>` : links `<text channel>` to `<voice channel>` (the text channel is visible only to the members in the voice channel)\n- `{self.bot.prefix}voicelink del <text channel>` : unlink `<text channel>` from its linked voice channel\n- `{self.bot.prefix}voicelink list` : list all existing links.\n- `{self.bot.prefix}voicelink resync [<text channel>|all]` : resynchronize the permissions for the given text channel (or for all currently linked channels) based on the linked voice channel (use in case of glitches). This will briefly remove and readd everyone to the channel.")))
        await ctx.send(_("A text channel can only be linked to one voice channel, but multiple text channels can be linked to the same voice channel. When a link is created, the bot will turn off visibility for `@everyone` if needed, and add the people in the voice channel. When a link is destroyed, it will just remove everyone and keep the channel private.\n*Please do not manually set/delete permissions for individual members for a text channel with an active link, or they will be destroyed by the bot.* The bot will not touch permissions for roles one way or another, so if you want to give some people permanent access to the text channel or add/deny specific permissions, give them a role and give the role permissions on the channel."))


async def setup(bot):
    global _
    _ = bot.themes.get_default_translator() # Get system/default translator
    await bot.add_cog(TieTextVoiceCog(bot))
