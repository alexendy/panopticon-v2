import discord
import typing
from discord.ext import commands, tasks

import random

from panopticon2.utils import *
from panopticon2.checks import has_gm_rights, GMRightsNeeded

_ = str


pagination_increment = 10


def get_p10(degradation:float):
    return 0.1 # 1 every 10 chars seems nice

def get_p01(degradation:float):
    if(degradation <1):
        raise ValueError(_("`degradation` must be a value of 1 or more. (If you want no degradation, just don't pass any `degradation` parameter.)"))
    else:
        return 1.0/degradation

class TwoStatesMarkovProcess:
    def __init__(self, p10:float, p01:float, initial_state:bool=True):
        self.p01 = p01
        self.p10 = p10
        self.state = initial_state
    
    def step(self):
        if(self.state): # True state
            if(random.random() < self.p10): # Switch to False
                self.state = False
        else: # False state
            if(random.random() < self.p01): # Switch to True
                self.state = True
        return self.state
    
    def steps(self, n:int):
        return [self.step() for _ in range(n)]
        
    
    def degrade_text(self, to_degrade:str, replacement_character='.', escape_markdown_and_spaces:bool=True):
        def replace_char(c:str):
            if escape_markdown_and_spaces and c in ["_","*","`",">"," ","\n","\t"]:
                return c
            else:
                return c if self.step() else replacement_character
        return ''.join([replace_char(c) for c in to_degrade])



def hidename_switch(argument):
    if argument in ["hide_name", "hide_names", "cacher_noms", "cacher_nom"]:
        return True
    elif argument in ["show_name", "show_names", "montrer_noms", "montrer_nom"]:
        return False
    else:
        raise commands.BadArgument("Should be hide_names or show_names") # No need to translate ; the sitch is is typing.Optional so this exception will be discarded without user feedback anyway


class SpyingCog(commands.Cog, name="Spying", description=_("Make the bot repeat to a user everything that happens in a channel.")):
    def __init__(self, bot):
        global _
        self.bot = bot
        self.bot.loop.create_task(self.setup()) # For setup that needs an initialized bot
    
    
    async def setup(self):
        await self.bot.wait_until_ready()
        self.bot.add_help(None, self.help_gm)
        


    #Low-level DB functions
    async def get_spyed_channel(self, gid:int, uid:int):
        record = await self.bot.mongo["server_"+str(gid)].spying.find_one({'uid': uid})
        if record:
            return record["chid"]
        else:
            return None

    async def get_spies(self, gid:int, chid:int):
        return [r async for r in self.bot.mongo["server_"+str(gid)].spying.find({'chid': chid})]


    async def get_all_spyings(self, gid:int):
        return [r async for r in self.bot.mongo["server_"+str(gid)].spying.find()]

        
    async def create_spying(self, gid:int, chid:int, uid:int, hide_names:bool=False, p_degrade:float=0., p_restore:float=1.):
        existing = await self.get_spyed_channel(gid, uid)
        if existing:
            return False
        await self.bot.mongo["server_"+str(gid)].spying.insert_one({'chid':chid, 'uid': uid, "hide_names":hide_names, "markov_p01":p_restore, "markov_p10":p_degrade, "markov_listen_state":True, "voices":[]})
        return True

    async def remove_spying(self, gid:int, uid:int):
        result = await self.bot.mongo["server_"+str(gid)].spying.delete_one({'uid': uid})
        return (result.deleted_count > 0) # True if we did delete something
    
    
    # Listener
    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.bot:
            return # Ignore bots (myself and others)
        guild = message.guild
        if not guild:
            return # DM
        gid = guild.id
        spies = await self.get_spies(gid, message.channel.id)
        if not spies:
            return # No spies
        _ = await self.bot.get_current_translation_gid(gid)
        for spying in spies:
            spying_touched = False
            authorid = message.author.id
            if spying["hide_names"] and not self.bot.is_gm(authorid, gid): # Handle name masking. GMs should never be masked
                try:
                    index = spying["voices"].index(authorid)+1
                except ValueError:
                    spying["voices"].append(authorid)
                    index = len(spying["voices"])
                    spying_touched = True
                authorlabel = f(_("Voice {index}"))
            else:
                authorlabel = message.author.display_name
            # Degrade message if needed
            if spying["markov_p10"] == 0.: # Can never degrade 
                processedmessage = message.clean_content
            else:
                #Create degrader
                degrader = TwoStatesMarkovProcess(spying["markov_p10"], spying["markov_p01"], spying["markov_listen_state"])
                oldmarkovstate = spying["markov_listen_state"]
                processedmessage = degrader.degrade_text(message.clean_content)
                if degrader.state != oldmarkovstate:
                    spying["markov_listen_state"] = degrader.state
                    spying_touched = True
            # Get spy
            spy = guild.get_member(spying["uid"])
            await spy.send(f(_("**(:spy:) {authorlabel} :** {processedmessage}")))
            if spying_touched:
                await self.bot.mongo["server_"+str(gid)].spying.replace_one({'_id': spying['_id']}, spying)
    
    
    # Commands
    @commands.group(invoke_without_command=True, aliases=["spy","espionner","espionnage"])
    async def spying(self, ctx):
        _ = await self.bot.get_current_translation_ctx(ctx)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        await ctx.send(f(_("Use `{self.bot.prefix}spying add <player> <channel> [hide_names|show_names] [<degradation>]` to create a spying, `{self.bot.prefix}spying stop <player>` to stop a spying, `{self.bot.prefix}spying list` to list all current spyings.")))


    async def format_spying(self, gid, spying:dict) -> str:
        _ = await self.bot.get_current_translation_gid(gid)
        guild = self.bot.get_guild(gid)
        spy = guild.get_member(spying["uid"])
        channel = guild.get_channel(spying["chid"])
        names_hidden = (_("names are hidden") if spying["hide_names"] else _("names are visible"))
        return f(_("**{spy.display_name}** is spying **{channel.name}** ({names_hidden})"))
        


    @spying.command(aliases=["liste"])
    async def list(self, ctx):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        spyings = await self.get_all_spyings(gid)
        if not spyings:
            await ctx.send(f(_("There is currently no spying going on in this game.")))
            return
        else:
            await ctx.send(f(_("__Current spyings:__")))
            for s in spyings:
                await ctx.send(await self.format_spying(gid, s))

    @spying.command(aliases=["ajouter","créer","creer"])
    async def add(self, ctx, player:str, channel:str, hide_names:typing.Optional[hidename_switch]=False, degradation:typing.Optional[float]=None):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        guild = self.bot.get_guild(gid)
        spied_channel = find_channel_type(guild, channel, discord.TextChannel)
        if not spied_channel:
            await ctx.send(f(_("{channel} does not exist or is not a text channel")))
            return
        spy = self.bot.get_member(gid, player)
        if not spy:
            await ctx.send(f(_("Unknown player {player}")))
            return
        if degradation: # Do math to get Markov process parameters from the user input
            try:
                p01 = get_p01(degradation)
                p10 = get_p10(degradation)
            except ValueError as err:
                await ctx.send(str(err))
                return
        else:
            p01 = 1.
            p10 = 0.
        # All sanity checks OK !
        outcome = await self.create_spying(gid, spied_channel.id, spy.id, hide_names, p_degrade=p10, p_restore=p01)
        if outcome: # OK !
            names_hidden = (_("names are hidden") if hide_names else _("names are visible"))
            degradation_str = (_("no degradation") if not degradation else f(_("degradation: {degradation}")))
            await ctx.send(f(_(":spy: **{spy.display_name}** is now spying **{spied_channel.name}** ({names_hidden}; {degradation_str})")))
            await spy.send(_(":spy: **You are now spying a text channel**"))
            await self.bot.log(gid, f(_(":spy: **{spy.display_name}** is now spying **{spied_channel.name}** ({names_hidden}; {degradation_str})")), ctx.author)
        else:
            await ctx.send(f(_("User {spy.display_name} is already spying a channel. Only one channel can be spied at a time.")))

    @spying.command(aliases=["delete","del","arrêter","arreter","supprimer","enlever"])
    async def stop(self, ctx, player:str):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        guild = self.bot.get_guild(gid)
        spy = self.bot.get_member(gid, player)
        if not spy:
            await ctx.send(f(_("Unknown player {player}")))
            return
        # All sanity checks OK !
        outcome = await self.remove_spying(gid, spy.id)
        if outcome: # OK !
            await ctx.send(f(_(":spy::no_entry_sign: **{spy.display_name}** is no longer spying.")))
            await spy.send(_(":spy: **You have stopped spying a text channel**"))
            await self.bot.log(gid, f(_(":spy::no_entry_sign: **{spy.display_name}** is no longer spying.**")), ctx.author)
        else:
            await ctx.send(f(_("User {spy.display_name} is not spying any channel.")))

    @commands.command(aliases=["test_dégradation"])
    async def test_degradation(self, ctx, degradation:float, *, test_message:str):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        if not await self.bot.is_gm_ctx(ctx):
            raise GMRightsNeeded()
        try:
            p01 = get_p01(degradation)
            p10 = get_p10(degradation)
        except ValueError as err:
            await ctx.send(str(err))
            return
        degrader = TwoStatesMarkovProcess(p10, p01)
        degraded_message = degrader.degrade_text(test_message)
        await ctx.send(f(_("Degraded text exemple for degradation={degradation} (Markov process probabilities p01={p01}, p10={p10}):\n>>> {degraded_message}.")))



    @spying.error
    @list.error
    @add.error
    @stop.error
    @test_degradation.error
    async def error_codename(self, ctx, error):
        _ = await self.bot.get_current_translation_ctx(ctx)
        if isinstance(error, GMRightsNeeded):
            await ctx.send(f(_("You need gamemaster rights to do this")))
        else:
            await ctx.send(f(_("Unknown error : {error}")))
            raise error
    
    
    
    async def help_gm(self, ctx):
        gid = await self.bot.get_current_game_ctx(ctx)
        _ = await self.bot.get_current_translation_gid(gid)
        await ctx.send(f(_("__Channel spying :mage::__\nYou can setup the bot to have it repeat all that happens in a channel to a player, in private messages. This can be used for when a character is spying on a scene without the participants knowing it. A character can only spy on one channel at once (but a channel can have multiple spies). The commands are:\n- `{self.bot.prefix}spying add <player> <channel> [hide_names|show_names] [<degradation>]`: have the player `<player>` spy on the channel `<channel>`.\n  * Passing the optional `hide_names` argument makes the names of the participants unknown to the spy (they will be replaced by \"Voice 1\", \"Voice 2\", etc.), this can be used e.g. if the spy can only listen but not see, or does not recognize the participants. The default is to show names. \n  * Passing an optional `<degradation>` numeric argument will randomly degrade the copied text, replacing part of it with dots. You can test degradation rates with the `$test_degradation` command (see below). The default is no degradation.")))
        await ctx.send(f(_("- `{self.bot.prefix}spying stop <player>`: stops the spying by `<player>`, if any.\n- `{self.bot.prefix}spying list`: lists all current spying in the game.\n- `{self.bot.prefix}test_degradation <degradation> <test message>`: allows you to test degradation rates: this will send you back `<test message>` degraded as if it had been spied with `<degradation>` degradation rate. The value must be at least 1. 1 is minimal degradation (just a character here and there). 10 is about half the text being lost. Higher numbers lead to heavy degradation. Keep in mind that degradation is random, so it may randomly be milder or worse sometimes.")))


 



async def setup(bot):
    global _
    _ = bot.themes.get_default_translator() # Get system/default translator
    await bot.add_cog(SpyingCog(bot))
