import discord
from discord.ext import commands, tasks

from panopticon2.utils import *

_ = str


class TestCog(commands.Cog, name=_("Test"), description=_("Test module for Panopticon v2")):
    def __init__(self, bot):
        global _
        self.bot = bot
        self.bot.loop.create_task(self.setup()) # For setup that needs an initialized bot
        self.my_message = _("Test message !")
    
    
    async def setup(self):
        await self.bot.wait_until_ready()
        self.bot.add_help(self.help_regular, self.help_gm)
        print(_("Test extension setup."))
        
    @commands.command()
    async def test(self, ctx):
        await ctx.send(f(_("Hello from test extension {ctx.author.display_name}. Here is the test message: {self.my_message}")))

    async def help_regular(self, ctx):
        await ctx.send(f(_("__Test extension:__\nThis is shown to non-GM users.")))

    async def help_gm(self, ctx):
        await ctx.send(f(_("__Test extension:__\nThis is shown to GM users :mage:.")))


async def setup(bot):
    global _
    _ = bot.themes.get_default_translator() # Get system/default translator
    await bot.add_cog(TestCog(bot))
