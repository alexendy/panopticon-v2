
import gettext



themes = ["generic-fr", "vampire-v5-fr"]

default = "generic-fr"

basepath = "./themes"



class ThemesManager:
    def __init__(self):
        self.translations = dict()
        self.default = default
        for theme in themes:
            try:
                self.translations[theme] = gettext.translation('pano-v2', localedir=basepath, languages=[theme])
                print(f"Translations loaded for theme : {theme}")
            except OSError:
                print(f"No translations for {theme} could be found, theme disabled")
        if self.default in self.translations:
            print(f"Default/system theme is : {self.default}")
        else:
            print(f"WARNING: default/system theme {self.default} is missing or could not be loaded, system messages and default bot text will not be translated.")
    
    def get_translator(self, theme):
        if theme in self.translations:
            return self.translations[theme].gettext
        else:
            return lambda x:x # identity
    
    def translate(self, theme, text):
        if theme in self.translations:
            return self.translations[theme].gettext(text)
        else:
            return text # identity
    
    def get_default_translator(self):
        return self.get_translator(self.default)
