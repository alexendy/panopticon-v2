import asyncio
import motor.motor_asyncio, pymongo
import discord
from discord.ext import commands
import logging
import random
import typing
import sys

from panopticon2 import __version__

from panopticon2.utils import *
from panopticon2.checks import has_gm_rights, has_botmaster_rights, GMRightsNeeded, BotmasterRightsNeeded, NoGameError, GameNotRunningError
from panopticon2.themes import ThemesManager

# Core - the heart of the bot gamemaster

# Handles :
# - Setting up new games, etc.
# - String game adaptation
# - Game-specific stuff



_ = str # Init the gettext _ as identity before ThemesManager is setup


async def check_theme(bot,ctx,id):
    theme = str(id)
    if theme not in bot.themes.translations:
        await ctx.send(f(_("Theme {theme} unknown ; defaulting to system theme {bot.themes.default}")))
        themes_list = ', '.join([f"`{k}`" for k in bot.themes.translations.keys()])
        await ctx.send(f(_("Available themes: {themes_list}")))
        return bot.themes.default
    else:
        return theme

# Description, conversion, print, default
# Note : the _'ed strings will not be translated here (at this stage, _ == str), but this marks them as translatable for xgettext, and they are translated in the 
gameinfo_structure = {
    "name": (_("Name of the game"), lambda bot, ctx, id: to_string(ctx,id), lambda ctx, id: to_string(ctx,id), _("My new game")),
    "tag": (_("Tag of the game (quick identifier, 5 characters min, for example 'v5montreal')"), lambda bot, ctx, id: to_valid_tag(ctx,id), lambda ctx, id: to_string(ctx,id), _("newgame")),
    "gm_id": (_("Main gamemaster"), lambda bot, ctx, id: to_member_id(ctx,id), lambda ctx, id: to_member(ctx,id), None),
    "gm_role_id": (_("Gamemaster role"), lambda bot, ctx, id: to_role_id(ctx,id), lambda ctx, id: to_role(ctx,id), None),
    "theme": (_("Bot theme"), lambda bot, ctx, id: check_theme(bot,ctx,id), lambda ctx, id: to_string(ctx,id), None),
    "log_channel_id": (_("ID of the logging channel"), lambda bot, ctx, id: to_channel_id(ctx,id), lambda ctx, id: to_channel(ctx,id), None),
    } # + "setup" which is True if setup is finished




class PanoClient(commands.Bot):
    def __init__(self, config):
        global _
        print(f(_("Panopticon v.{__version__} starting...")))
        self.is_init = False
        if not self.parse_config(config):
            print(_("Initialization failed, aborting."))
            sys.exit(1)
        
        self.themes = ThemesManager()
        
        _ = self.themes.get_default_translator()
        
        # Intents setup
        intents = discord.Intents.default()
        intents.members = True
        intents.messages = True
        intents.message_content = True
        
        # Initialize the bot
        super(PanoClient, self).__init__(command_prefix=self.prefix, description=_("Panopticon v2 - your friendly neighborhood virtual LARP gamemaster bot"), intents=intents, activity = discord.Game(name=f(_("Type {self.prefix}help in DM"))), help_command=None) #"{self.prefix}help pour de l'aide !"
        self.loaded_modules = []
        
        # Help coroutines for modules
        self.gm_help_modules = []
        self.regular_help_modules = []
        
        # Add base commands
        self.add_help_command()
        self.add_setup_commands()
        self.add_switch_commands()
        self.add_admin_commands()
        self.is_init = True
    
    def parse_config(self,config):
        global _
        try:
            version = config["version"]
            if(version < 1):
                print(f(_("WARNING: Config file with unknown version {version}, this may cause issues")))
            self.token = config["token"]
            self.prefix = config["prefix"]
            self.extensions_enabled = config["extensions"]
            self.mongodb_uri = config["mongodb_uri"]
            try:
                self.botmaster_id = config["botmaster_id"]
            except KeyError:
                self.botmaster_id = None
            return True
        except KeyError as k:
            print(f(_("Error : Missing critical configuration item {k}")))
            return False
            
    def launch(self):
        self.run(self.token)
        
        
    async def on_ready(self):
        global _
        self.mongo = motor.motor_asyncio.AsyncIOMotorClient(self.mongodb_uri)
        try:
            serverinfo = await self.mongo.server_info()
            v = serverinfo['version']
            print(f(_("Successfully connected to MongoDB server at {self.mongodb_uri} (server version {v})")))
        except pymongo.errors.ServerSelectionTimeoutError:
            print(f(_("Can't connect to MongoDB server at {self.mongodb_uri} !")))
            sys.exit(1)
        for module in self.extensions_enabled:
            print(f(_("Trying to load module {module}... ")), end='')
            try:
                await self.load_extension(f"panopticon2.modules.{module}")
                self.loaded_modules.append(module)
                print(f(_("OK")))
            except commands.errors.ExtensionNotFound:
                print(f(_("Module not found !")))
        if self.loaded_modules:
            modlist = ", ".join(self.loaded_modules)
            print(f(_("Modules loaded: {modlist}")))
        
        print(f(_("Panopticon2 v.{__version__} ready.")))
        #await self.broadcast_message(f(_("**__Panopticon2 v.{__version__} ready.__**\nStartup finished, system ready :robot::game_die:")))
    
    
    async def server_known(self, id: int) -> bool:
        dbs = await self.mongo.list_database_names()
        #print(dbs)
        for db in dbs:
            if(db == ("server_"+str(id))):
                return True
        return False
    
    async def server_ready(self, id: int) -> bool:
        dbs = await self.mongo.list_database_names()
        for db in dbs:
            if(db == ("server_"+str(id))):
                gameinfo = await self.mongo["server_"+str(id)].general.find_one()
                return gameinfo["setup"]
        return False
    
    async def server_in_setup(self, id: int) -> bool:
        return (await self.server_known(id)) and (not await self.server_ready(id))
    
    async def get_gameinfo(self, guild_id:int) -> typing.Optional[dict]:
        if not (await self.server_known(guild_id)):
            return None
        else:
            gameinfo = await self.mongo["server_"+str(guild_id)].general.find_one()
            #print(gameinfo)
            return gameinfo

    # === Multi-game utility functions ===
    #TODO : change index key for self.mongo["master_index"].users !!!! the uid is not unique !
    # Registers a user as a participant to a the game running on a server
    # Returns True if the user was added, or False 
    async def register_player_game(self, user_id:int, game_guild_id:int, also_set_current:bool=False):
        global _
        if not await self.server_ready(game_guild_id):
            raise RuntimeError(_("You can only register players for games for which the server setup is finished."))
        record = await self.mongo["master_index"].users.find_one({'uid': user_id,'gid': game_guild_id})
        if record is None:
            await self.mongo["master_index"].users.insert_one({'uid': user_id,'gid': game_guild_id, 'current': also_set_current}) # Current identifies the "current" game, i.e. the one implicitly referred to when speaking to the bot in DM
            if(also_set_current):
                await self.mongo["master_index"].users.update_many({'uid': {"$eq" : user_id}, 'gid': {"$ne":game_guild_id}}, {'$set':{'current': False}}) #Remove the "current" tag from any other game
            await self.mongo["server_"+str(game_guild_id)].users.insert_one({'_id': user_id}) # UIDs are all Discord -unique, so no collision problem
            return True
        else:
            return False # Did not create anything
    
    async def set_current_game(self, user_id:int, game_guild_id:int):
        await self.mongo["master_index"].users.update_many({'uid': {"$eq" : user_id}, 'gid': {"$ne":game_guild_id}}, {'$set':{'current': False}})
        await self.mongo["master_index"].users.update_many({'uid': {"$eq" : user_id}, 'gid': {"$eq":game_guild_id}}, {'$set':{'current': True}})
    
    
    async def get_games(self, user_id:int):
        ids = [r["gid"] async for r in self.mongo["master_index"].users.find({'uid': user_id})]
        return ids

    async def get_current_game(self, user_id:int):
        record = await self.mongo["master_index"].users.find_one({'uid': user_id, 'current':True})
        if record is None:
            return None # No current game
        else:
            return record["gid"]
    
    async def get_gid_by_tag(self, tag:str) -> typing.Optional[int]:
        record = await self.mongo["master_index"].games_by_tag.find_one({'tag':tag})
        if record is None:
            return None # Unknown game
        else:
            return record["gid"]
    
    # Gets the game ID corresponding to the context :
    # - On a guild, it is the guild ID if a game is setup on that guild (or None otherwise)
    # - In DM, it is the current game of the author (if any)
    async def get_current_game_ctx(self, ctx:commands.Context):
        if ctx.guild:
            if await self.server_known(ctx.guild.id):
                return ctx.guild.id
            else:
                return None
        else:
            return await self.get_current_game(ctx.author.id)
    
    
    # Localization getters
    
    # Get the right localization for a game
    async def get_current_translation_gid(self, gid:int):
        info = await self.get_gameinfo(gid)
        if info is None:
            return self.themes.get_default_translator() # failsafe
        translator = self.themes.get_translator(info["theme"]) # this already fails gracefully
        return translator
    
    # Get the right localization for a user, i.e. that of the current game
    async def get_current_translation_uid(self, uid:int):
        current_gid = await self.get_current_game(uid)
        if current_gid is None:
            return self.themes.get_default_translator() # failsafe
        info = await self.get_gameinfo(current_gid)
        translator = self.themes.get_translator(info["theme"]) # this already fails gracefully
        return translator
    
    # Get the right localization for a  context (that of the server on a server, or of a user in DM)
    async def get_current_translation_ctx(self, ctx:commands.Context):
        if ctx.guild is None:
            return await self.get_current_translation_uid(ctx.author.id)
        else:
            return await self.get_current_translation_gid(ctx.guild.id)
    
    
    # Is someone the botmaster ?
    def is_botmaster(self, user_id:int) -> bool:
        return (user_id == self.botmaster_id)

    
    
    # Is someone GM on a game ? If no game is provided, do it on the current game
    async def is_gm(self, user_id:int, game_id:typing.Optional[int]) -> bool:
        if game_id is None:
            game_id = await self.get_current_game(user_id)
            if game_id is None:
                return False # No current game, no GM rights
        gamedata = await self.get_gameinfo(game_id)
        if gamedata is None: # No game here
            return False
        if user_id == gamedata["gm_id"]: # This user is the main GM
            return True
        # Otherwise : check role
        guild = self.get_guild(game_id) # Get the guild object
        member = guild.get_member(user_id) # Get the Member object
        gm_role = guild.get_role(gamedata["gm_role_id"]) #... and the Role object
        if gm_role in member.roles:
            return True
        else:
            return False
    
    # Same as previous with a Context : if in DM, works on the default game ; otherwise on the guild's game
    async def is_gm_ctx(self, ctx:commands.Context) -> bool:
        if ctx.guild:
            return await self.is_gm(ctx.author.id, ctx.guild.id)
        else:
            return await self.is_gm(ctx.author.id, None)
    
    
    # Utility function for uid <-> member conversions
    def get_member(self, gid:int, identifier: typing.Union[int, str]) -> discord.Member:
        guild = self.get_guild(gid) # Get the guild object
        member = None
        try: # First try to interpret it as UID
            uid = int(identifier)
            member = guild.get_member(uid)
        except ValueError: # This is not an int
            pass
        if member:
            return member
        else:
            #This is not an int or it was not found by uid, try by username/nick
            return guild.get_member_named(identifier)
    
    
    # Auto add member that join active servers to the running game
    async def on_member_join(self, member: discord.Member):
        global _
        if not await self.server_ready(member.guild.id):
            return # Do nothing for servers on which the bot is not setup (yet)
        else:
            gameinfo = await self.get_gameinfo(member.guild.id)
            gamename = gameinfo["name"]
            await member.send(f(_("Welcome on the server for the game {gamename}, {member.name} !")))
            await self.register_player_game(member.id, member.guild.id, True) #TODO : do not autoset as current for final/production version
    
    
    # Send a message to the log channel of a game:
    async def log(self, gid:int, message:str, author:typing.Optional[discord.Member]=None):
        info = await self.get_gameinfo(gid)
        if info is None or not info["setup"]: # Game unknown or not ready
            return False
        else:
            guild = self.get_guild(gid)
            logchannel = guild.get_channel(info["log_channel_id"])
            if author:
                await logchannel.send(f"(**{author.display_name}**) : {message}")
            else:
                await logchannel.send(message)
    
    # Send a message to all log channels of all guilds !
    async def broadcast_message(self, message:str, tag_gm=False):
        errors = list()
        n_success = 0
        n_ignored = 0
        for g in self.guilds:
            info = await self.get_gameinfo(g.id)
            if info is None or not info["setup"]: # Game unknown or not ready
                n_ignored += 1
                continue
            logchannel = g.get_channel(info["log_channel_id"])
            
            if tag_gm:
                gm_role = g.get_role(info["gm_role_id"])
                tosend = gm_role.mention + " " + message
            else:
                tosend = message
            try:
                await logchannel.send(tosend)
                n_success += 1
            except discord.Forbidden:
                errors.append(g.name)
        return (n_success, n_ignored, errors)
    
    
    # Help
    def add_help_command(self):
        @self.command(aliases=["statut", "help"])
        @commands.dm_only()
        async def hello(ctx):
            # NOT global _ : _ must change halfway through the help !
            _ = self.themes.get_default_translator() # Start with default/system theme
            uid = ctx.author.id
            games = await self.get_games(uid)
            # If no games, just say so and exit
            if not games:
                await ctx.send(f(_("Hello, {ctx.author.name}. You currently do not take part to any game handled by this service :no_entry_sign::game_die:")))
                return
            # There are games, list them
            gamenames = []
            for gid in games:
                info = await self.get_gameinfo(gid)
                gamenames.append(info["name"]+ " (`"+info["tag"]+"`)")
            gamestr = ", ".join(gamenames)
            await ctx.send(f(_("Hello, {ctx.author.name}. You are currently part of the following game(s) :game_die:: {gamestr}")))
            # Fetch current game
            current_game_id = await self.get_current_game(uid)
            if len(gamenames) > 1:
                await ctx.send(f(_("To change the selected game, use `{self.prefix}switch <gametag>`")))
            # If no current game, stop here
            if current_game_id is None:
                await ctx.send(_("No game is currently selected."))
                return
            # There is a current game
            gameinfo = await self.get_gameinfo(current_game_id)
            #Switch to game locale
            _ = await self.get_current_translation_ctx(ctx)
            gm_rights = await self.is_gm(uid, current_game_id)
            gamename = gameinfo['name']
            await ctx.send(f(_("\n\n**The currently selected game is: __{gamename}__.**"))+ (_(" **You are a gamemaster ! :mage:**") if gm_rights else ""))
            if(gm_rights):
                for helpfunc in self.gm_help_modules:
                    await helpfunc(ctx)
            else:
                for helpfunc in self.regular_help_modules:
                    await helpfunc(ctx)
        
        @hello.error
        async def error_help(ctx, error):
            global _
            if isinstance(error, commands.errors.PrivateMessageOnly):
                await ctx.send(f(_("Please use this command in DM.")))
            else:
                await ctx.send(f(_("Unknown error : {error}")))
                raise error
    
    # Hooks for modules:
    def add_help(self, regular_help, gm_help):
        if regular_help:
            self.regular_help_modules.append(regular_help)
        if gm_help:
            self.gm_help_modules.append(gm_help)
        
    
    #Multi-game handling commands
    def add_switch_commands(self):
        global _
        @self.command(aliases=["gameswitch", "select", "gameselect"])
        @commands.dm_only()
        async def switch(ctx, gametag:str):
            gid = await self.get_gid_by_tag(gametag)
            if gid is None:
                await ctx.send(f(_("No known game with tag `{gametag}`.")))
                return
            # Is ctx.author in this game ?
            games = await self.get_games(ctx.author.id)
            if gid not in games:
                await ctx.send(_("You are not part of this game."))
                return
            # All good, let's switch
            await self.set_current_game(ctx.author.id, gid)
            info = await self.get_gameinfo(gid)
            gamename = info["name"]
            await ctx.send(f(_("**{gamename}** is now the currently selected game ! *DMs with the bot will refer to that game.*")))
            
        @switch.error
        async def switch_error(ctx, error):
            if isinstance(error, commands.errors.PrivateMessageOnly):
                await ctx.send(f(_("Please use this command in DM. The active game is only relevant to DMs, if you talk with the bot on a server it will always refer to the game on the current server.")))
            else:
                await ctx.send(f(_("Unknown error : {error}")))
    
    
    #Botmaster commands
    def add_admin_commands(self):
        global _
        @self.command(aliases=["broadcast", "broadcast_message"])
        @has_botmaster_rights(self)
        async def service_message(ctx, *, message:str):
            to_send = f(_("**__:loudspeaker: Bot operator service message :__** {message}"))
            (n_success, n_ignored, errors) = await self.broadcast_message(to_send, tag_gm=True)
            n_errors = len(errors)
            errorstr = " (" + (", ".join(errors)) + ")" if n_errors > 0 else ""
            await ctx.send(f(_(":loudspeaker: Service message successfully sent to {n_success} servers. ({n_ignored} were not found or ignored, {n_errors} raised an error{errorstr})")))
        
        @service_message.error
        async def botmaster_error(ctx, error):
            if isinstance(error, BotmasterRightsNeeded):
                await ctx.send(f(_("Only the bot operator can use this command.")))
            else:
                await ctx.send(f(_("Unknown error : {error}")))
    
        
    # Setup commands
    def add_setup_commands(self):
        global _
        @self.group(brief=_("Sets up the bot on a new server"), description=_("Launches the server setup process if this server is not already known."), invoke_without_command=True)
        @commands.guild_only()
        async def setup(ctx):
            if (await self.server_known(ctx.guild.id)):
                await ctx.send(_("The bot is already set up (or being set up) for this server"))
            else:
                await ctx.send(_("**Bot setup started for this server.** You will be the main gamemaster."))
                initial_gameinfo = {key:default for (key,(paramname, conversion, printer, default)) in gameinfo_structure.items()}
                initial_gameinfo["gm_id"] = ctx.author.id # Set the author as the (initial) GM
                initial_gameinfo["theme"] = self.themes.default # Default to system/default theme
                initial_gameinfo["name"] = ctx.guild.name # Default to server name
                initial_gameinfo["tag"] = None # Default to server name
                initial_gameinfo["setup"] = False # The server is not setup yet
                await self.mongo["server_"+str(ctx.guild.id)].general.insert_one(initial_gameinfo)
                await ctx.send(f(_("Use `{self.prefix}setup print` to print the current setup, `{self.prefix}setup set <parameter> <value>` to set a parameter, `{self.prefix}setup finish` to finish the setup and launch the game.")))
        
        @setup.command()
        @has_gm_rights(self)
        @commands.guild_only()
        async def set(ctx, param:str, value):
            gid = ctx.guild.id
            if(await self.server_in_setup(gid)):
                gameinfo = await self.get_gameinfo(gid)
                if param not in gameinfo_structure:
                    await ctx.send(f(_("Unknown parameter {param}")))
                else:
                    paramname, conversion, printer, default = gameinfo_structure[param]
                    translatedname = _(paramname)
                    try:
                        cvalue = await conversion(self, ctx, value)
                        gameinfo[param] = cvalue
                        await self.mongo["server_"+str(gid)].general.replace_one({'_id': gameinfo['_id']}, gameinfo)
                        newval = await printer(ctx,cvalue)
                        await ctx.send(f(_("`{param}` ({translatedname}) is now set to **{newval}**")))
                    except ValueError:
                        await ctx.send(f(_("Wrong type for parameter `{param}` (should be {conversion.__name__})")))
                    except commands.BadArgument as e:
                        await ctx.send(f(_("Invalid argument for parameter `{param}` : {e}")))
            else:
                await ctx.send(_("Critical parameters can only be changed during game setup."))
        
        @setup.command(aliases=["print","info"])
        @has_gm_rights(self)
        @commands.guild_only()
        async def printinfo(ctx):
            gid = ctx.guild.id
            if(await self.server_known(gid)):
                gameinfo = await self.get_gameinfo(gid)
                await ctx.send(_("**Current game parameters:**"))
                for (k,(paramname, conversion, printer, default)) in gameinfo_structure.items():
                    val = await printer(ctx,gameinfo[k])
                    translatedname = _(paramname)
                    await ctx.send(f" - {translatedname} (`{k}`) : {val}")
                themes_list = ', '.join([f"`{k}`" for k in self.themes.translations.keys()])
                await ctx.send(f(_("Available themes: {themes_list}")))
            else:
                await ctx.send(f(_("No game is known on this server; run `{self.prefix}setup` to setup a new game.")))
        
        @setup.command()
        @has_gm_rights(self)
        @commands.guild_only()
        async def finish(ctx):
            gid = ctx.guild.id
            if(await self.server_in_setup(gid)):
                gameinfo = await self.get_gameinfo(gid)
                allgood = True
                if gameinfo["gm_role_id"] is None:
                    allgood = False
                    await ctx.send(_("Please set a GM role (`gm_role_id`)"))
                if gameinfo["log_channel_id"] is None:
                    allgood = False
                    await ctx.send(_("Please set a bot log channel for this server (`log_channel_id`)"))
                if gameinfo["tag"] is None:
                    allgood = False
                    await ctx.send(_("Please set a game tag (`tag`)"))
                else:
                    # Check tag unicity
                    game_tag_check = await self.get_gid_by_tag(gameinfo["tag"])
                    if game_tag_check is not None:
                        allgood = False
                        gametag = gameinfo["tag"]
                        await ctx.send(f(_("Game tag `{gametag}` is already used by another game, please set another one.")))
                if allgood:
                    await ctx.send(_("Server setup complete ! "))
                    gameinfo["setup"] = True
                    await self.mongo["server_"+str(gid)].general.replace_one({'_id': gameinfo['_id']}, gameinfo)
                    await self.mongo["master_index"].games_by_tag.insert_one({'gid': gid, 'tag':gameinfo["tag"]})
                    await ctx.send(_("Registering all current non-bot users..."))
                    count_users = 0
                    for m in ctx.guild.members:
                        if not m.bot:
                            if await self.register_player_game(m.id, gid, True): #TODO : do not autoset as current for final/production version
                                count_users += 1
                    await ctx.send(f(_("Done ; {count_users} registered.")))
            else:
                await ctx.send(_("Server must be in setup for setup to be finished."))
        
        @setup.error
        @printinfo.error
        @set.error
        async def error_setup(ctx, error):
            if isinstance(error, GMRightsNeeded):
                await ctx.send(f(_("You need gamemaster rights to do this")))
            elif isinstance(error, commands.errors.NoPrivateMessage):
                await ctx.send(f(_("You can only run setup commands on a server, not in DM.")))
            elif isinstance(error, commands.errors.PrivateMessageOnly):
                await ctx.send(f(_("You can only run this in DM, not on a server.")))
            else: 
                await ctx.send(f(_("Unknown error : {error}")))
                raise error
