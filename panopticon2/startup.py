from panopticon2.core import PanoClient


def start(config):
    bot = PanoClient(config)
    if bot.is_init:
        bot.launch()

