#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Miranda Coninx
    ISIR - Sorbonne Universite / CNRS
    17/02/2022
""" 

import asyncio
import discord
from discord.ext import commands 

# Conversions

async def to_user(ctx, identifier):
    if identifier is None:
        return None
    conv = commands.UserConverter()
    return await conv.convert(ctx, str(identifier))

async def to_member(ctx, identifier):
    if identifier is None:
        return None
    conv = commands.MemberConverter()
    return await conv.convert(ctx, str(identifier))

async def to_role(ctx, identifier):
    if identifier is None:
        return None
    conv = commands.RoleConverter()
    return await conv.convert(ctx, str(identifier))

async def to_channel(ctx, identifier):
    if identifier is None:
        return None
    conv = commands.TextChannelConverter()
    return await conv.convert(ctx, str(identifier))

async def to_user_id(ctx, identifier):
    value = await to_user(ctx, identifier)
    if value is None:
        return None
    else:
        return value.id

async def to_member_id(ctx, identifier):
    value = await to_member(ctx, identifier)
    if value is None:
        return None
    else:
        return value.id

async def to_role_id(ctx, identifier):
    value = await to_role(ctx, identifier)
    if value is None:
        return None
    else:
        return value.id

async def to_channel_id(ctx, identifier):
    value = await to_channel(ctx, identifier)
    if value is None:
        return None
    else:
        return value.id


async def to_string(ctx, identifier):
    if identifier is None:
        return None
    return str(identifier)

async def to_valid_tag(ctx, identifier, seps=['\n','\t','\r',' ',"'"],minlen=5):
    if identifier is None:
        return None
    rv = str(identifier).lower()
    for s in seps:
        rv = rv.replace(s,'-')
    if len(rv) < minlen: # Not long enough
        return None
    return rv




def find_channel(guild:discord.Guild, identifier:str):
    try:
        chid = int(identifier)
        found = guild.get_channel(chid)
        if found:
            return found
    except ValueError:
        pass
    for channel in guild.channels:
        if identifier == channel.name:
            return channel

def find_channel_type(guild:discord.Guild, identifier:str, chtype):
    channel = find_channel(guild,identifier)
    if isinstance(channel, chtype):
        return channel
    else:
        return None







# Poor man's f-string
# To use with gettext
# https://stackoverflow.com/questions/49797658/how-to-use-gettext-with-python-3-6-f-strings/56264202
from inspect import currentframe
from copy import copy

def f(s):
    frame = currentframe().f_back
#    return eval(f"f'{s}'", frame.f_locals, frame.f_globals)
    kwargs = copy(frame.f_globals)
    kwargs.update(frame.f_locals)
    return s.format(**kwargs)



