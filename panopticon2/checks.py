#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Miranda Coninx
    ISIR - Sorbonne Universite / CNRS
    16/02/2022
""" 

import asyncio
import discord
from discord.ext import commands 

from panopticon2.utils import *


class GMRightsNeeded(commands.CheckFailure): # You need GM rights
    pass

class BotmasterRightsNeeded(commands.CheckFailure): # You need GM rights
    pass

class NoGameError(commands.CheckFailure): # There is no game known on this server
    pass

class GameNotRunningError(commands.CheckFailure): # There is no game running on this server
    pass

#def has_gm_rights(bot):
#    async def predicate(ctx):
#        if ctx.guild is None: # Command in DM
#            return False
#        gamedata = await bot.get_gameinfo(ctx.guild.id)
#        if gamedata is None: # No game here
#            return False
#        gm_role = await to_role(ctx, gamedata["gm_role_id"])
#        gm_member = await to_member(ctx, gamedata["gm_id"])
#        if ctx.author == gm_member:
#            return True # This is the main GM
#        if gm_role in ctx.author.roles:
#            return True # Has the GM role
#        raise GMRightsNeeded() # Otherwise, deny access
#    return commands.check(predicate)
    

def has_gm_rights(bot):
    async def predicate(ctx):
        gameid = None if ctx.guild is None else ctx.guild.id
        if not await bot.is_gm(ctx.author.id, gameid):
            raise GMRightsNeeded()
        return True
    return commands.check(predicate)

def has_botmaster_rights(bot):
    async def predicate(ctx):
        if not bot.is_botmaster(ctx.author.id):
            raise BotmasterRightsNeeded()
        return True
    return commands.check(predicate)
