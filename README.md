# panopticon V2

Modular, multi-game bot for virtual LARP


## Project Goals
Goal for panopticon V2
- Modular design :
  * A core that just directly handle
    + Different games w. server and GM roles/IDs
    + Some operator users that can manage games and GMs. Nothing in a JSON config except API key and operator IDs.
    + Some server setup process (for the log channel, enabled modules)
    + The logging console for GMs
    + A DB to store all persistent info
    + Per-game text strings (similar to internationalization/cultural adaptation)
  * Modules : planned ones (by decreasing priority)
    + Anonymous messaging + codenames -> DONE
      - With delayed messages -> DONE
    + Link text channels to voice channels -> DONE
    + Channel spying (i.e. the Obfuscate problem) -> DONE
    + Bank (may integrate with codenames)
    + Some way to pull and display character sheet info from some remote backend/webservice ?
    + Some way for users to enter daily actions, with per-player eligible actions (determined how ?) and submit them to some backend (report for GMs, webservice, etc.)
    + Some way to help the GM monitor the channels and DMs, maybe create specific DMs with the bot and the GM(s) and monitor activity


## Prerequisites
- Reasonably modern Python 3 with modules:
  - `discord.py`
  - `aiofiles`
  - `pymongo`
  - `motor`
- A [MongoDB](https://www.mongodb.com/) database somewhere.
  - The simplest way to get one is to use the official Docker container :
  ```shell
  docker pull mongo:latest
  docker run --name mongo-pano2 -p 27017:27017 -v pano2-db:/data/db --restart unless-stopped -d mongo:latest
  ```
  Be careful that it has **no security**  by default, so run it on a secured host and/or setup access control !

## How to use

### Bot setup
- Create a bot on [https://discord.com/developers/], and invite it on your server with an OAuth2 link.
  - The simplest is to give the bot admin rights. Anyway, it should only be invited on specific virtual LARP servers that will only be used for gaming.
- Edit the `config.json` file to :
  * Set the bot token and the path to the MongoDB database
  * Change the prefix if you want
  * If you want to disable some extensions, do so
  * Set the ID of the botmaster : this should be *your* personal Discord UID as the person running the bot. Being the botmaster does not give you any rights on any game, it just allows you to send service messages to the GMs of all active games.
- Start the bot with `python3 start.py`

### Server specific setup
(We will assume the bot prefix is `$`, if you changed it for something else, please adapt the instructions.)
- You should set up on the server:
  - A GM role, that all GMs will have. The bot will use this role to recognize the GMs
  - A text channel that will be used as a log console by the bot. Plenty of secret stuff will be written by the bot on this console, so it should only be accessible to the bot and GMs.
- After the bot is invited in a server, type `$setup` on any channel to initiate setup. You must then set some parameters:
  - `$setup set name <game name>` : Choose the full name of the game. By default, it is the name of the server.
  - `$setup set tag <game tag>` : Choose the game tag. This is a short label, without spaces, at least 5 characters long, that will identify the game. It is mostly used to switch between games.
  - `$setup set gm_role_id <gm role>` : Set the GM role.
  - `$setup set gm_id <gm role>` : Set the identity of the main GM. That person will always be considered a GM even if it doesn't have the GM role. By default, this is the person who initially typed `$setup`. If you change it during configuration, the game "ownership" will be transfered to that user and they will have to finish the setup.
  - `$setup set log_channel_id <log channel>` : Set the bot log console
  - `$setup set theme <theme>` : Set the bot theme for the game. The available themes can be listed with `$setup print`
- Once everything is good, run `$setup finish` to end setup and start the game.
- The bot will "register" all the current non-bot users of the discord for the game, and all newcomers will also be automatically registered for now. This is mostly technical, for switching between several games. It does not require any management for the GM, and it is no big deal if some accounts that are not part of the final game are registered. 


## How to create your own themes

TODO


## License
All this is licensed under GPLv3.
