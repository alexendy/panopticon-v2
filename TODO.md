# Missing features in existing modules

## Core
- Allowthe GMs  to modify some core parameters (name, tag, GMs, theme) in a running game
- Some way for a botmaster or main GM to close a game.
- Some way to cancel an ongoing server setup
- Ability to pause and resume a game
- Ability for a GM to kick or ban people from a game

## Messaging
- The bot should mention the game the message relates to if it is not the current game
- Allow multiple recipients ?
- Find a way to allow pictures ?

## Spying
- The bot should mention the game the message relates to if it is not the current game
- Allow multiple concurrent spyings with non-confusing output

## Codenames
- The bot should mention the game the message relates to if it is not the current game
